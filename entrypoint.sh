#!/bin/bash

export APP_CONFIG=production

source .env

if [[ -f /.passwords ]]; then
  source /.passwords
fi
if [[ -f /.config ]]; then
  source /.config
fi

python manage.py runserver