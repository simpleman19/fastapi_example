FROM python:3.9-slim

RUN apt-get update && apt-get install -y gcc mediainfo

ARG APP_ROOT=/usr/src/app

EXPOSE 8000

RUN mkdir -p ${APP_ROOT}
WORKDIR ${APP_ROOT}

COPY requirements.txt ${APP_ROOT}
RUN pip install --no-cache-dir -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${APP_ROOT}

RUN ./dumpenv.sh

# Start server
CMD ["./entrypoint.sh"]
