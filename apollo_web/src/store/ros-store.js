import { createStore } from './vue-state.js';
import { LogIn } from '../api/generated';
import { authApiBuilder } from '@/api/wrapper';


export default createStore({
  failure: false,

  async login(username, password) {
    this.logout();
    authApiBuilder().postLogin(new LogIn(username, password), (error, data, resp) => {
      console.log(resp)
      if (resp.statusCode !== 200) {
        console.log('Failure');
        this.failure = true;
      } else {
        this.token = data.token;
        this.username = username;
      }
    });
  },

  isLoggedIn() {
    return this.username !== null && this.username !== '' && this.token !== null && this.token !== '';
  },

  didLoginFail() {
    return this.failure;
  },
});
