import { createStore } from './vue-state.js';
import { LogIn } from '../api/generated';
import { authApiBuilder } from '@/api/wrapper';

export const AUTH_TOKEN_LOCAL_STORAGE_KEY = 'authJwt';
export const AUTH_USERNAME_LOCAL_STORAGE_KEY = 'authUsername';

export default createStore({
  username: localStorage.getItem(AUTH_TOKEN_LOCAL_STORAGE_KEY),
  token: localStorage.getItem(AUTH_USERNAME_LOCAL_STORAGE_KEY),
  failure: false,

  async login(username, password) {
    this.logout();
    authApiBuilder().postLogin(new LogIn(username, password), (error, data, resp) => {
      console.log(resp)
      if (resp.statusCode !== 200) {
        console.log('Failure');
        this.failure = true;
      } else {
        this.token = data.token;
        this.username = username;
        localStorage.setItem(AUTH_TOKEN_LOCAL_STORAGE_KEY, data.token);
        localStorage.setItem(AUTH_USERNAME_LOCAL_STORAGE_KEY, username);
      }
    });
  },

  isLoggedIn() {
    return this.username !== null && this.username !== '' && this.token !== null && this.token !== '';
  },

  didLoginFail() {
    return this.failure;
  },

  logout() {
    this.failure = false;
    this.username = null;
    this.token = null;
    localStorage.removeItem(AUTH_TOKEN_LOCAL_STORAGE_KEY);
    localStorage.removeItem(AUTH_USERNAME_LOCAL_STORAGE_KEY);
  },
});
