import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';

const routes = [
  {
    path: '/',
    name: 'HOME',
    component: Home,
  },
  {
    path: '/about',
    name: 'ABOUT',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/login',
    name: 'LOGIN',
    component: () => import(/* webpackChunkName: "login" */ '../views/Auth/Login.vue'),
  },
  {
    path: '/logout',
    name: 'LOGOUT',
    component: () => import(/* webpackChunkName: "logout" */ '../views/Auth/Logout.vue'),
  },
  {
    path: '/account/dashboard',
    name: 'DASHBOARD',
    component: () => import(/* webpackChunkName: "account_home" */ '../views/Account/Dashboard.vue'),
  },
  {
    path: '/register',
    name: 'REGISTER',
    component: () => import(/* webpackChunkName: "register" */ '../views/About.vue'),
  },
  {
    path: '/ros/:id',
    name: 'ROSVIEW',
    component: () => import(/* webpackChunkName: "rosview" */ '../views/ROS/RosView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
