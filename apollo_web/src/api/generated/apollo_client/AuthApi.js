/**
 * Apollo API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: dev
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import HTTPValidationError from '../model/HTTPValidationError';
import LogIn from '../model/LogIn';
import SingleUserOut from '../model/SingleUserOut';
import TokenOut from '../model/TokenOut';
import UpdatePass from '../model/UpdatePass';
import WebAuthUserModelsUserCreate from '../model/WebAuthUserModelsUserCreate';

/**
* Auth service.
* @module apollo_client/AuthApi
* @version dev
*/
export default class AuthApi {

    /**
    * Constructs a new AuthApi. 
    * @alias module:apollo_client/AuthApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the getJwtIdentity operation.
     * @callback module:apollo_client/AuthApi~getJwtIdentityCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SingleUserOut} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Jwt Identity
     * @param {module:apollo_client/AuthApi~getJwtIdentityCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SingleUserOut}
     */
    getJwtIdentity(callback) {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTBearer'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = SingleUserOut;
      return this.apiClient.callApi(
        '/v1/auth/me', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the postCreateUser operation.
     * @callback module:apollo_client/AuthApi~postCreateUserCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SingleUserOut} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create User
     * @param {module:model/WebAuthUserModelsUserCreate} webAuthUserModelsUserCreate 
     * @param {module:apollo_client/AuthApi~postCreateUserCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SingleUserOut}
     */
    postCreateUser(webAuthUserModelsUserCreate, callback) {
      let postBody = webAuthUserModelsUserCreate;
      // verify the required parameter 'webAuthUserModelsUserCreate' is set
      if (webAuthUserModelsUserCreate === undefined || webAuthUserModelsUserCreate === null) {
        throw new Error("Missing the required parameter 'webAuthUserModelsUserCreate' when calling postCreateUser");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = SingleUserOut;
      return this.apiClient.callApi(
        '/v1/auth/create', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the postLogin operation.
     * @callback module:apollo_client/AuthApi~postLoginCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TokenOut} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Login
     * @param {module:model/LogIn} logIn 
     * @param {module:apollo_client/AuthApi~postLoginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TokenOut}
     */
    postLogin(logIn, callback) {
      let postBody = logIn;
      // verify the required parameter 'logIn' is set
      if (logIn === undefined || logIn === null) {
        throw new Error("Missing the required parameter 'logIn' when calling postLogin");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = TokenOut;
      return this.apiClient.callApi(
        '/v1/auth/login', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the postRefreshJwt operation.
     * @callback module:apollo_client/AuthApi~postRefreshJwtCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TokenOut} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Refresh Jwt
     * @param {module:apollo_client/AuthApi~postRefreshJwtCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TokenOut}
     */
    postRefreshJwt(callback) {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTBearer'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = TokenOut;
      return this.apiClient.callApi(
        '/v1/auth/refresh', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the postUpdateUserPassword operation.
     * @callback module:apollo_client/AuthApi~postUpdateUserPasswordCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SingleUserOut} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update User Password
     * @param {module:model/UpdatePass} updatePass 
     * @param {module:apollo_client/AuthApi~postUpdateUserPasswordCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SingleUserOut}
     */
    postUpdateUserPassword(updatePass, callback) {
      let postBody = updatePass;
      // verify the required parameter 'updatePass' is set
      if (updatePass === undefined || updatePass === null) {
        throw new Error("Missing the required parameter 'updatePass' when calling postUpdateUserPassword");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = SingleUserOut;
      return this.apiClient.callApi(
        '/v1/auth/password', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
