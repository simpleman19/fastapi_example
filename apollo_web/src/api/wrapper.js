import { ApiClient, AuthApi } from './generated';

export const AUTH_TOKEN_LOCAL_STORAGE_KEY = 'authJwt';

let defaultClient = ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
defaultClient.authentications['JWTBearer'].accessToken = localStorage.getItem(AUTH_TOKEN_LOCAL_STORAGE_KEY);
defaultClient.basePath = process.env.VUE_APP_API_LOCATION;

export const authApiBuilder = () => {
  return new AuthApi();
};

