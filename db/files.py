from datetime import datetime
from tempfile import TemporaryFile
from typing import Union, Optional, Dict, TypeVar, Generic

from gridfs import NoFile
from pydantic import Field

from common.utils.db import PyObjectId, clean_mongo_id
from common.utils.files import sanitize_filename, get_filename_ext, hash_file
from . import build_mongo_db, BaseDbModel, BasePydanticModel, BasePydanticConfig
from motor import motor_asyncio, motor_gridfs

BUF_SIZE = 65536 * 2


class FileMetadata(BasePydanticModel):
    sha256: str
    file_ext: str
    is_public: bool = Field(default=False)
    uploaded_by: Optional[PyObjectId]

    class Config(BasePydanticConfig):
        title = 'FileModel__Metadata'


class FileModel(BaseDbModel):
    uploadDate: Optional[datetime]
    filename: str
    chunkSize: int
    length: int
    metadata: FileMetadata

    class Response(BasePydanticModel):
        id: Optional[PyObjectId] = Field(alias='_id')
        uploadDate: Optional[datetime]
        filename: str
        chunkSize: int
        length: int
        metadata: FileMetadata

        class Config(BasePydanticConfig):
            title = 'FileModel__Response'


MODEL = TypeVar('MODEL')
META = TypeVar('META')


class FileHelper(Generic[MODEL, META]):
    def __init__(self, name, model_class, meta_class):
        self.collection = build_mongo_db(name)
        self.fs: motor_gridfs.AgnosticGridFSBucket = motor_asyncio.AsyncIOMotorGridFSBucket(self.collection)
        self.model_class = model_class
        self.meta_class = meta_class

    async def download_stream(self, file_id=Union[PyObjectId, str]) -> Optional[motor_gridfs.AgnosticGridOut]:
        if isinstance(file_id, str):
            file_id = PyObjectId(file_id)
        try:
            return await self.fs.open_download_stream(file_id)
        except NoFile:
            return None

    async def upload_from_file(self, filename, file: TemporaryFile, extra=None):
        if extra is None:
            extra = {}
        filename = sanitize_filename(filename)
        file.seek(0)
        _hash = hash_file(file)
        file.seek(0)
        meta = self.meta_class(**extra, sha256=_hash, file_ext=get_filename_ext(filename))
        return await self.fs.upload_from_stream(filename, source=file, metadata=meta.dict(by_alias=True))

    async def find_by_query(self, query: Dict, limit: int = 100):
        docs = []
        cursor = self.fs.find(query).sort("uploadDate", -1)
        for document in await cursor.to_list(length=limit):
            docs.append(self.model_class(**document))
        return docs

    async def find_by_query_single(self, query: Dict):
        cursor = self.fs.find(query).sort("uploadDate", -1)
        for document in await cursor.to_list(length=1):
            return self.model_class(**document)
        return None

    async def get_upload_stream_by_id(
            self,
            _id: Union[str, PyObjectId],
            filename: str) -> motor_gridfs.AgnosticGridIn:
        _id = clean_mongo_id(_id)
        return self.fs.open_upload_stream_with_id(_id, filename)

    async def update_metadata(self, _id: Union[str, PyObjectId], metadata: META):
        _id = clean_mongo_id(_id)
        files = getattr(self.collection, 'fs.files')
        return await files.update_one({'_id': _id}, {'$set': {'metadata': metadata.dict(by_alias=True)}})


class FileHelperFactory:
    @staticmethod
    def build(collection_name, model_class=FileModel, meta_class=FileMetadata):
        return FileHelper(collection_name, model_class, meta_class)
