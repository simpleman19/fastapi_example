from typing import Union
from fastapi import HTTPException

from common.service import InjectableService, InjectableServiceAuthRequired
from common.utils.db import PyObjectId
from db.models.terminal.repo import PendingTerminal, pending_terminal_helper, PendingTerminalDbFilter, terminal_helper, \
    TerminalDbFilter, Terminal, TerminalType


class PendingTerminalService(InjectableService):
    @staticmethod
    async def register_pending_terminal(terminal_id: str) -> PendingTerminal:
        found = await pending_terminal_helper.find_by_query_single(PendingTerminalDbFilter(terminal_id=terminal_id))
        if found:
            raise HTTPException(405, detail="Terminal already exists in pending table.")
        found = await terminal_helper.find_by_query_single(TerminalDbFilter(terminal_id=terminal_id))
        if found:
            raise HTTPException(405, detail="Terminal already claimed by user.")
        pending_term = PendingTerminal(terminal_id=terminal_id)
        return await pending_terminal_helper.insert_one(pending_term)


class TerminalService(InjectableServiceAuthRequired):
    async def claim_pending_terminal(self, terminal_id: str, terminal_type: TerminalType) -> Terminal:
        found = await terminal_helper.find_by_query_single(TerminalDbFilter(terminal_id=terminal_id))
        if found:
            raise HTTPException(405, detail="Terminal already claimed by user")

        pending_term = await pending_terminal_helper.find_by_query_single(
            PendingTerminalDbFilter(terminal_id=terminal_id))
        if not pending_term:
            raise HTTPException(404, detail="Failed to find pending terminal")

        user = await self.get_user()
        new_term = Terminal(
            owner_id=user.owner_id,
            terminal_id=terminal_id,
            type=terminal_type
        )
        term = await terminal_helper.insert_one(new_term)
        if term:
            await pending_terminal_helper.actually_delete(pending_term.id)
        return term

    async def my_terminals(self):
        user = await self.get_user()
        return await terminal_helper.find_by_query(TerminalDbFilter(owner_id=user.owner_id))

    async def get_terminal(self, _id: Union[str, PyObjectId]):
        user = await self.get_user()
        return await terminal_helper.find_by_query_single(TerminalDbFilter(owner_id=user.owner_id, id=_id))

    async def update_term(self, _id: Union[str, PyObjectId], update: Terminal.Update):
        user = await self.get_user()
        term = await terminal_helper.find_by_query_single(TerminalDbFilter(owner_id=user.owner_id, id=_id))
        if not term:
            raise HTTPException(404, detail='Terminal Not Found')
        if term.owner_id != user.owner_id:
            raise HTTPException(401, detail='Unauthorized.')
        return await terminal_helper.update_one(update, _id)
