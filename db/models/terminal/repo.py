from datetime import datetime
from enum import Enum
from typing import Optional, Dict, List, Union

from pydantic import Field

from common.utils.db import PyObjectId, BasicDbFilter
from db import BaseDbModel, DbHelper, DbHelperFactory, BasePydanticModel, BasePydanticConfig

COLLECTION_NAME = 'terminal'


class TerminalDbFilter(BasicDbFilter):
    _extended_col_filters = {'owner_id', 'terminal_id'}
    terminal_id: Optional[str]
    owner_id: Optional[Union[str, PyObjectId]]


class TerminalType(str, Enum):
    CONTROL = 'Control'


class ContentType(str, Enum):
    RUN_OF_SHOW = 'RunOfShow'


class DisplayContent(BasePydanticModel):
    x: int = Field(default=0)
    y: int = Field(default=0)
    width: int = Field(default=1280)
    height: int = Field(default=720)
    type: ContentType = Field(default=ContentType.RUN_OF_SHOW)


class DisplayConfig(BasePydanticModel):
    width: int = Field(default=1280)
    height: int = Field(default=720)
    windowed: bool = Field(default=True)
    contents: List[DisplayContent] = Field(default=[])
    display_id: str = Field(default='')


class TerminalConfig(BasePydanticModel):
    displays: Dict[int, DisplayConfig] = Field(default={})


class Terminal(BaseDbModel):
    owner_id: PyObjectId
    terminal_id: str
    secret_hash: Optional[str] = Field(default=None)
    name: str = Field(default='Untitled')
    type: TerminalType = Field()
    config: TerminalConfig = Field(default_factory=TerminalConfig)

    class Update(BasePydanticModel):
        name: Optional[str]
        type: Optional[TerminalType]
        config: Optional[TerminalConfig]

        class Config(BasePydanticConfig):
            title = 'Terminal__Update'

    class Response(BasePydanticModel):
        id: Optional[PyObjectId] = Field(alias='_id')
        name: str
        terminal_id: str
        type: TerminalType = Field()
        creation_timestamp: datetime
        config: TerminalConfig

        class Config(BasePydanticConfig):
            title = 'Terminal__Response'

    class ResponseSecret(BasePydanticModel):
        id: Optional[PyObjectId] = Field(alias='_id')
        name: str
        secret: str
        terminal_id: str
        type: TerminalType = Field()
        creation_timestamp: datetime
        config: TerminalConfig

        class Config(BasePydanticConfig):
            title = 'Terminal__ResponseSecret'


terminal_helper: DbHelper[Terminal] = DbHelperFactory.build(COLLECTION_NAME, Terminal)

PENDING_COLLECTION_NAME = 'pending_terminal'


class PendingTerminalDbFilter(BasicDbFilter):
    _extended_col_filters = {'terminal_id'}
    terminal_id: Optional[str]


class PendingTerminal(BaseDbModel):
    terminal_id: str

    class Response(BasePydanticModel):
        id: Optional[PyObjectId] = Field(alias='_id')
        terminal_id: str

        class Config(BasePydanticConfig):
            title = 'PendingTerminal__Response'


pending_terminal_helper: DbHelper[PendingTerminal] = DbHelperFactory.build(PENDING_COLLECTION_NAME, PendingTerminal)
