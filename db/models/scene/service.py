from typing import List

from common.service import InjectableServiceAuthRequired
from db.models.scene.repo import Scene, scene_helper, SceneDbFilter


class SceneService(InjectableServiceAuthRequired):
    async def get_my_scenes(self) -> List[Scene]:
        user = await self.get_user()
        db_filter = SceneDbFilter.default()
        db_filter.owner_id = user.owner_id
        return await scene_helper.find_by_query(db_filter)

    async def create_scene(self, scene):
        user = await self.get_user()
        new_scene = Scene(owner_id=user.owner_id, **scene.dict())
        return await scene_helper.insert_one(new_scene)

    async def get_scene(self, _id: str):
        user = await self.get_user()
        db_filter = SceneDbFilter.default()
        db_filter.owner_id = user.owner_id
        db_filter.id = _id
        return await scene_helper.find_by_query_single(db_filter)
