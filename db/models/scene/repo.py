from datetime import datetime
from typing import Optional, Dict, Union

from pydantic import Field

from common.utils.db import PyObjectId, BasicDbFilter
from db import BaseDbModel, DbHelper, DbHelperFactory, BasePydanticModel, BasePydanticConfig

COLLECTION_NAME = 'scene'


class SceneDbFilter(BasicDbFilter):
    _extended_col_filters = {'owner_id'}
    owner_id: Optional[Union[str, PyObjectId]]


class Scene(BaseDbModel):
    owner_id: PyObjectId
    name: str = Field(default='Untitled')
    desc: str
    start_time: datetime = Field(default_factory=datetime.now)
    location: str

    class Create(BasePydanticModel):
        name: str
        desc: Optional[str] = Field(default='')
        location: Optional[str] = Field(default='')

        class Config(BasePydanticConfig):
            title = 'Scene__Create'

    class Update(BasePydanticModel):
        id: PyObjectId = Field(alias='_id')
        name: Optional[str]

        class Config(BasePydanticConfig):
            title = 'Scene__Update'

    class Response(BasePydanticModel):
        id: Optional[PyObjectId] = Field(alias='_id')
        name: str
        desc: str
        start_time: datetime
        location: str

        class Config(BasePydanticConfig):
            title = 'Scene__Response'


scene_helper: DbHelper[Scene] = DbHelperFactory.build(COLLECTION_NAME, Scene)
