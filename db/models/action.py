from typing import Dict, List, Union

from pydantic import BaseModel, Field


class ApolloAction(BaseModel):
    action_type: str = Field(default='NOP')
    command: Dict[str, Union[str, Dict, List]] = Field(default={})
    delay_millis: int = Field(default=0)
