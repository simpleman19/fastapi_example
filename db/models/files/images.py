import io
import tempfile
from typing import Union, Optional, Dict
from PIL import Image
from fastapi import HTTPException, UploadFile
from motor import motor_gridfs
from pydantic import Field

from common.service import InjectableServiceAuthRequired
from common.utils.db import PyObjectId, clean_mongo_id
from db.files import FileHelperFactory, FileModel, FileMetadata, FileHelper
from db import BasePydanticModel


class ImageSized(BasePydanticModel):
    image_id: Optional[PyObjectId]
    width: int
    height: int


class ImageMetadata(FileMetadata):
    width: int
    height: int
    other_sizes: Dict[str, ImageSized] = Field(default={})
    file_ext: str


class ImageModel(FileModel):
    metadata: ImageMetadata

    class Response(FileModel.Response):
        metadata: ImageMetadata

        class Config(FileModel.Response.Config):
            title = 'ImageModel__Response'


images_helper: FileHelper[ImageModel, ImageMetadata] = FileHelperFactory.build('images', ImageModel, ImageMetadata)


async def get_resized_image_id(image: ImageModel, width, height):
    if (width or height) and (image.metadata.width != width or image.metadata.height != height):
        if not width:
            width = int(round(image.metadata.width * height / image.metadata.height, 0))
        elif not height:
            height = int(round(image.metadata.height * width / image.metadata.width, 0))

        correct_size = image.metadata.other_sizes.get(f"{width}x{height}")
        if not correct_size:
            image_stream = await images_helper.download_stream(image.id)
            file = await image_stream.read()
            tmp_img = Image.open(io.BytesIO(file))
            tmp_img = tmp_img.resize((width, height))
            with tempfile.NamedTemporaryFile(mode='w+b', suffix=image.metadata.file_ext) as tmp_file:
                tmp_img.save(tmp_file)
                tmp_file.seek(0)
                new_image_id = await images_helper.upload_from_file(
                    image.filename,
                    tmp_file,
                    {
                        'is_public': image.metadata.is_public,
                        'uploaded_by': image.metadata.uploaded_by,
                        'width': tmp_img.width,
                        'height': tmp_img.height
                    }
                )
                correct_image_id = new_image_id
                image.metadata.other_sizes[f"{width}x{height}"] = ImageSized(
                    image_id=new_image_id,
                    width=width,
                    height=height,
                    file_ext=image.metadata.file_ext
                )
                await images_helper.update_metadata(image.id, image.metadata)
        else:
            correct_image_id = correct_size.image_id
    else:
        correct_image_id = image.id
    return correct_image_id


class PublicImageService:
    @staticmethod
    async def get_image(_id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        image = await images_helper.find_by_query_single(
            {
                '_id': _id,
                'metadata.is_public': True
            }
        )
        if not image:
            raise HTTPException(status_code=404, detail="Image not found.")
        return image

    @staticmethod
    async def get_image_stream(
            _id: Union[str, PyObjectId],
            width: Optional[int] = None,
            height: Optional[int] = None) -> Optional[motor_gridfs.AgnosticGridOut]:
        image: Optional[ImageModel] = await PublicImageService.get_image(_id)
        if image:
            correct_image_id = await get_resized_image_id(image, width, height)
            image_stream = await images_helper.download_stream(correct_image_id)
            if not image_stream:
                raise HTTPException(status_code=404, detail="Image file not found.")
            return image_stream
        else:
            raise HTTPException(status_code=404, detail="Image not found.")


class ImageService(InjectableServiceAuthRequired):
    async def get_image(self, _id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        image = await images_helper.find_by_query_single(
            {
                '_id': _id,
            },
        )
        if not image:
            raise HTTPException(status_code=404, detail="Image not found in dealership.")
        return image

    async def get_image_stream(
            self,
            _id: Union[str, PyObjectId],
            width: Optional[int] = None,
            height: Optional[int] = None) -> Optional[motor_gridfs.AgnosticGridOut]:
        image = await self.get_image(_id)
        if image:
            correct_image_id = await get_resized_image_id(image, width, height)
            image_stream = await images_helper.download_stream(correct_image_id)
            if not image_stream:
                raise HTTPException(status_code=404, detail="Image file not found in dealership.")
            return image_stream
        else:
            raise HTTPException(status_code=404, detail="Image not found in dealership.")

    async def save_image(self, file: UploadFile, public: bool):
        user = await self.kernel.get_user()

        pil_img = Image.open(file.file)
        if not pil_img:
            raise HTTPException(status_code=406, detail='Unprocessable Image Provided.')

        return await images_helper.upload_from_file(
            file.filename,
            file.file,
            {
                'is_public': public,
                'uploaded_by': user.id,
                'width': pil_img.width,
                'height': pil_img.height
            }
        )

    async def make_public(self, pic_id: Union[str, PyObjectId], public: bool):
        img: ImageModel = await self.get_image(pic_id)
        img.metadata.is_public = public
        await images_helper.update_metadata(pic_id, img.metadata)
        for i in img.metadata.other_sizes.values():
            await images_helper.update_metadata(i.image_id, img.metadata)
