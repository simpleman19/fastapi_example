from typing import Union, Optional

from fastapi import HTTPException, UploadFile
from motor import motor_gridfs
from pydantic import Field
from starlette.requests import Request

from common.service import InjectableServiceAuthRequired
from common.utils.db import PyObjectId, clean_mongo_id
from common.utils.files import GridOutResponse
from common.utils.videos import get_media_info_for_file
from db.files import FileHelperFactory, FileModel, FileMetadata, FileHelper


class VideoMetadata(FileMetadata):
    width: int
    height: int
    duration: Optional[float] = Field(default=0)


class VideoModel(FileModel):
    metadata: VideoMetadata

    class Response(FileModel.Response):
        metadata: VideoMetadata

        class Config(FileModel.Response.Config):
            title = 'VideoModel__Response'


video_helper: FileHelper[VideoModel, VideoMetadata] = FileHelperFactory.build('video', VideoModel, VideoMetadata)


class PublicVideoService:
    @staticmethod
    async def get_video(_id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        video = await video_helper.find_by_query_single(
            {
                '_id': _id,
                'metadata.is_public': True
            }
        )
        if not video:
            raise HTTPException(status_code=404, detail="Video not found.")
        return video

    @staticmethod
    async def get_video_stream(_id: Union[str, PyObjectId]) -> Optional[motor_gridfs.AgnosticGridOut]:
        video = await PublicVideoService.get_video(_id)
        if video:
            vid_stream = await video_helper.download_stream(video.id)
            if not vid_stream:
                raise HTTPException(status_code=404, detail="Video file not found.")
            return vid_stream
        else:
            raise HTTPException(status_code=404, detail="Video not found.")


class VideoService(InjectableServiceAuthRequired):
    async def get_video(self, _id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        video = await video_helper.find_by_query_single(
            {
                '_id': _id,
            }
        )
        if not video:
            raise HTTPException(status_code=404, detail="Video not found in dealership.")
        return video

    async def get_video_stream(self, _id: Union[str, PyObjectId]) -> Optional[motor_gridfs.AgnosticGridOut]:
        video = await self.get_video(_id)
        if video:
            vid_stream = await video_helper.download_stream(video.id)
            if not vid_stream:
                raise HTTPException(status_code=404, detail="Video file not found in dealership.")
            return vid_stream
        else:
            raise HTTPException(status_code=404, detail="Video not found in dealership.")

    async def save_video(self, file: UploadFile, public: bool = False):
        user = await self.kernel.get_user()

        info = await get_media_info_for_file(file.filename, file.file)
        if not info:
            raise HTTPException(status_code=406, detail='Unprocessable Video Provided.')
        width = info.get('video', {}).get('width', -1)
        height = info.get('video', {}).get('height', -1)
        duration = info.get('video', {}).get('duration', -1)
        return await video_helper.upload_from_file(
            file.filename,
            file.file,
            {
                'is_public': public,
                'uploaded_by': user.id,
                'width': width,
                'height': height,
                'duration': duration,
            }
        )


BYTES_PER_RESPONSE = 1000000


async def build_vid_response(grid_out: motor_gridfs.AgnosticGridOut, req: Request) -> GridOutResponse:
    asked = req.headers.get("Range")
    if asked:
        start_byte_requested = int(asked.split("=")[-1][:-1])
    else:
        start_byte_requested = 0

    end_byte_planned = min(start_byte_requested + BYTES_PER_RESPONSE, grid_out.length)

    return GridOutResponse(grid_out, attachment=False, start_byte=start_byte_requested,
                           end_byte=end_byte_planned)
