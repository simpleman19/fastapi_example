from typing import Union, Optional

from fastapi import HTTPException, UploadFile
from motor import motor_gridfs

from common.service import InjectableServiceAuthRequired
from common.utils.db import PyObjectId, clean_mongo_id
from db.files import FileHelperFactory, FileModel, FileMetadata, FileHelper


class AttachmentMetadata(FileMetadata):
    pass


class AttachmentModel(FileModel):
    metadata: AttachmentMetadata

    class Response(FileModel.Response):
        metadata: AttachmentMetadata

        class Config(FileModel.Response.Config):
            title = 'AttachmentModel__Response'


attachment_helper: FileHelper[AttachmentModel, AttachmentMetadata] = FileHelperFactory.build(
    'attachment',
    AttachmentModel,
    AttachmentMetadata
)


class PublicAttachmentService:
    @staticmethod
    async def get_attachment(_id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        attach = await attachment_helper.find_by_query_single(
            {
                '_id': _id,
                'metadata.is_public': True
            }
        )
        if not attach:
            raise HTTPException(status_code=404, detail="Attachment not found.")
        return attach

    @staticmethod
    async def get_attachment_stream(_id: Union[str, PyObjectId]) -> Optional[motor_gridfs.AgnosticGridOut]:
        attach = await PublicAttachmentService.get_attachment(_id)
        if attach:
            vid_stream = await attachment_helper.download_stream(attach.id)
            if not vid_stream:
                raise HTTPException(status_code=404, detail="Attachment file not found.")
            return vid_stream
        else:
            raise HTTPException(status_code=404, detail="Attachment not found.")


class AttachmentService(InjectableServiceAuthRequired):
    async def get_attachment(self, _id: Union[str, PyObjectId]):
        _id = clean_mongo_id(_id)
        attach = await attachment_helper.find_by_query_single(
            {
                '_id': _id,
            }
        )
        if not attach:
            raise HTTPException(status_code=404, detail="Attachment not found in dealership.")
        return attach

    async def get_attachment_stream(self, _id: Union[str, PyObjectId]) -> Optional[motor_gridfs.AgnosticGridOut]:
        attach = await self.get_attachment(_id)
        if attach:
            attach_stream = await attachment_helper.download_stream(attach.id)
            if not attach_stream:
                raise HTTPException(status_code=404, detail="Attachment file not found in dealership.")
            return attach_stream
        else:
            raise HTTPException(status_code=404, detail="Attachment not found in dealership.")

    async def save_attachment(self, file: UploadFile, public: bool = False):
        user = await self.kernel.get_user()

        return await attachment_helper.upload_from_file(
            file.filename,
            file.file,
            {
                'is_public': public,
                'uploaded_by': user.id,
            }
        )
