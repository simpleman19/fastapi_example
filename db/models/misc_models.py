from typing import Optional

from pydantic import Field

from db import BasePydanticModel, BasePydanticConfig


class GenericAddress(BasePydanticModel):
    addr_1: Optional[str] = Field(default='')
    addr_2: Optional[str] = Field(default='')
    city: Optional[str] = Field(default='')
    state: Optional[str] = Field(default='')
    zipcode: Optional[str] = Field(default='')
    country: Optional[str] = Field(default='')

    class Config(BasePydanticConfig):
        title = 'GenericAddress'
