from typing import Optional, Dict, Union, List

from pydantic import Field, BaseModel

from common.utils.db import PyObjectId, BasicDbFilter
from db import BaseDbModel, BasePydanticConfig, DbHelper, DbHelperFactory, BasePydanticModel
from db.models.action import ApolloAction

COLLECTION_NAME = 'run_of_show'


class RosDbFilter(BasicDbFilter):
    _extended_col_filters = {'scene_id', 'owner_id'}
    scene_id: Optional[Union[str, PyObjectId]]
    owner_id: Optional[Union[str, PyObjectId]]


class RosEntry(BaseModel):
    desc: str = Field(default='Entry')
    actions: Dict[int, ApolloAction] = Field(default={})


class RunOfShow(BaseDbModel):
    owner_id: PyObjectId
    scene_id: PyObjectId
    name: str = Field(default='Untitled')
    desc: str = Field(default='')
    entries: Dict[int, RosEntry] = Field(default={})

    class Create(BaseModel):
        name: str
        desc: Optional[str]

        class Config(BasePydanticConfig):
            title = 'RunOfShow__Create'

    class Update(BasePydanticModel):
        id: PyObjectId = Field(alias='_id')
        name: Optional[str]
        desc: Optional[str]

        class Config(BasePydanticConfig):
            title = 'RunOfShow__Update'

    class RosAppendEntry(BasePydanticModel):
        entry: RosEntry

        class Config(BasePydanticConfig):
            title = 'RunOfShow__AppendEntry'

    class RosUpdateEntries(BasePydanticModel):
        entries: Dict[int, RosEntry]

        class Config(BasePydanticConfig):
            title = 'RunOfShow__UpdateEntries'

    class Response(BasePydanticModel):
        id: PyObjectId = Field(alias='_id')
        owner_id: PyObjectId
        scene_id: PyObjectId
        name: str
        desc: str
        entries: Dict[int, RosEntry]

        class Config(BasePydanticConfig):
            title = 'RunOfShow__Response'


ros_helper: DbHelper[RunOfShow] = DbHelperFactory.build(COLLECTION_NAME, RunOfShow)
