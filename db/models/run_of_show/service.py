from typing import List, Union

from fastapi import Depends

from common.fastapi_kernel import KernelAuthRequired
from common.service import InjectableServiceAuthRequired
from common.utils.db import PyObjectId, clean_mongo_id
from db.models.run_of_show.repo import RunOfShow, ros_helper, RosDbFilter
from db.models.scene.service import SceneService


class RosService(InjectableServiceAuthRequired):
    def __init__(
            self,
            kernel: KernelAuthRequired = Depends(KernelAuthRequired),
            scene_service: SceneService = Depends(SceneService),
    ):
        super().__init__(kernel)
        self.scene_service = scene_service

    async def get_ros_for_scene(self, scene_id: Union[str, PyObjectId]) -> List[RunOfShow]:
        user = await self.get_user()
        await self.scene_service.get_scene(scene_id)
        db_filter = RosDbFilter.default()
        db_filter.owner_id = user.owner_id
        db_filter.scene_id = clean_mongo_id(scene_id)
        return await ros_helper.find_by_query(db_filter)

    async def create_ros(self, scene_id: Union[str, PyObjectId], ros: RunOfShow.Create):
        user = await self.get_user()
        await self.scene_service.get_scene(scene_id)
        new_ros = RunOfShow(owner_id=user.owner_id, scene_id=scene_id, **ros.dict())
        return await ros_helper.insert_one(new_ros)

    async def get_ros(self, scene_id: Union[str, PyObjectId], ros_id: Union[str, PyObjectId]):
        user = await self.get_user()
        await self.scene_service.get_scene(scene_id)
        db_filter = RosDbFilter.default()
        db_filter.owner_id = user.owner_id
        db_filter.scene_id = clean_mongo_id(scene_id)
        db_filter.id = ros_id
        return await ros_helper.find_by_query_single(db_filter)

    def update_ros(self, _id, ros_id, ros_update):
        # TODO
        pass

    def update_ros_entries(self, _id, ros_id, ros_entries):
        # TODO
        pass
