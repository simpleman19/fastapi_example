import enum
from datetime import datetime
from typing import Optional

from pydantic import Field, BaseModel

from common.utils.db import PyObjectId, BasicDbFilter
from db import BaseDbModel, BasePydanticConfig, DbHelper, DbHelperFactory, BasePydanticModel

COLLECTION_NAME = 'live_game'


class LiveGameDbFilter(BasicDbFilter):
    pass


class LiveGameModel(BaseDbModel):
    name: str = Field(default='')
    team1_text: str = Field(default='')
    team2_text: str = Field(default='')
    team3_text: str = Field(default='')
    team4_text: str = Field(default='')
    player1_text: str = Field(default='')
    player2_text: str = Field(default='')
    player3_text: str = Field(default='')
    player4_text: str = Field(default='')
    map_1: int = Field(default=0)
    map_2: int = Field(default=0)
    round_number: str = Field(default='')
    match_number: str = Field(default='')
    updated_timestamp: Optional[datetime] = Field(default_factory=datetime.now)

    class Create(BaseModel):
        name: Optional[str] = Field(default='')
        team1_text: Optional[str] = Field(default='')
        team2_text: Optional[str] = Field(default='')
        team3_text: Optional[str] = Field(default='')
        team4_text: Optional[str] = Field(default='')
        player1_text: Optional[str] = Field(default='')
        player2_text: Optional[str] = Field(default='')
        player3_text: Optional[str] = Field(default='')
        player4_text: Optional[str] = Field(default='')
        map_1: Optional[int] = Field(default=0)
        map_2: Optional[int] = Field(default=0)
        round_number: Optional[str] = Field(default='')
        match_number: Optional[str] = Field(default='')

        class Config(BasePydanticConfig):
            title = 'LiveGame_Create'

    class Update(BaseModel):
        name: Optional[str]
        team1_text: Optional[str]
        team2_text: Optional[str]
        team3_text: Optional[str]
        team4_text: Optional[str]
        player1_text: Optional[str]
        player2_text: Optional[str]
        player3_text: Optional[str]
        player4_text: Optional[str]
        map_1: Optional[int]
        map_2: Optional[int]
        round_number: Optional[str]
        match_number: Optional[str]
        updated_timestamp: Optional[datetime] = Field(default_factory=datetime.now)

        class Config(BasePydanticConfig):
            title = 'LiveGame_Update'

    class Response(BasePydanticModel):
        name: str
        team1_text: str
        team2_text: str
        team3_text: str
        team4_text: str
        player1_text: str
        player2_text: str
        player3_text: str
        player4_text: str
        map_1: int
        map_2: int
        round_number: str
        match_number: str
        updated_timestamp: datetime

        class Config(BasePydanticConfig):
            title = 'LiveGame_Response'

    def clear(self):
        self.team1_text = ''
        self.team2_text = ''
        self.team3_text = ''
        self.team4_text = ''
        self.player1_text = ''
        self.player2_text = ''
        self.player3_text = ''
        self.player4_text = ''
        self.map_1 = 0
        self.map_2 = 0
        self.round_number = ''
        self.match_number = ''


live_game_helper: DbHelper[LiveGameModel] = DbHelperFactory.build(COLLECTION_NAME, LiveGameModel)


class OrgTypes(str, enum.Enum):
    BATTLEFY = "battlefy"
    CUSTOM = "custom"


class OrgDbFilter(BasicDbFilter):
    pass


class OrganizationModel(BaseDbModel):
    created_by: PyObjectId
    btlfy_id: Optional[str] = Field()
    btlfy_slug: Optional[str] = Field()
    org_type: OrgTypes = Field(default=OrgTypes.CUSTOM)
    name: str = Field()

    # @staticmethod
    # def from_btlfy_json(json_dict):
    #     self.btlfy_id = json_dict.get('_id')
    #     self.btlfy_slug = json_dict.get('slug')
    #     self.name = json_dict.get('name')

    class Create(BaseModel):
        name: str
        org_type: OrgTypes
        btlfy_slug: Optional[str]

        class Config(BasePydanticConfig):
            title = 'Organization_Create'

    class Update(BaseModel):
        name: Optional[str]
        org_type: OrgTypes
        btlfy_slug: Optional[str]

        class Config(BasePydanticConfig):
            title = 'Organization_Update'

    class Response(BasePydanticModel):
        btlfy_id: Optional[str]
        btlfy_slug: Optional[str]
        org_type: OrgTypes
        name: str


org_helper: DbHelper[OrganizationModel] = DbHelperFactory.build('organization', OrganizationModel)
