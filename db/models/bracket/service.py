from typing import Dict, Union

from fastapi import HTTPException

from common.service import InjectableServiceAuthRequired
from common.utils.db import PyObjectId, clean_mongo_id
from db.models.bracket.repo import OrganizationModel, org_helper, OrgDbFilter, LiveGameDbFilter, live_game_helper, \
    LiveGameModel


class LiveGameService(InjectableServiceAuthRequired):
    async def get(self, _id: Union[str, PyObjectId]) -> LiveGameModel:
        db_filter = LiveGameDbFilter.default()
        db_filter.id = clean_mongo_id(_id)
        game = await live_game_helper.find_by_query_single(db_filter)
        if not game:
            raise HTTPException(status_code=404, detail="Live Game not found.")
        return game

    async def update(self, _id: Union[str, PyObjectId], game: LiveGameModel.Update) -> LiveGameModel:
        await self.get(_id)  # verify permissions and exists
        return await live_game_helper.update_one(game, _id)

    async def create(self, game: LiveGameModel.Create) -> LiveGameModel:
        new_game = LiveGameModel(**game.dict())
        return await live_game_helper.insert_one(new_game)

    async def get_live_games(self) -> Dict[str, LiveGameModel]:
        dealers = await live_game_helper.find_by_query({})
        return {str(d.id): d for d in dealers}

    async def create_live_games(self, count: int):
        for i in range(count):
            await self.create(LiveGameModel.Create())


class OrgService(InjectableServiceAuthRequired):
    async def get(self, _id: Union[str, PyObjectId]) -> OrganizationModel:
        db_filter = OrgDbFilter.default()
        db_filter.id = clean_mongo_id(_id)
        org = await org_helper.find_by_query_single(db_filter)
        if not org:
            raise HTTPException(status_code=404, detail="Car not found.")
        return org

    async def update(self, _id: Union[str, PyObjectId], org: OrganizationModel.Update) -> OrganizationModel:
        await self.get(_id)  # verify permissions and exists
        return await org_helper.update_one(org, _id)

    async def create(self, org: OrganizationModel.Create) -> OrganizationModel:
        user = await self.kernel.get_user()
        new_dealer = OrganizationModel(**org.dict(), created_by=user.id)
        return await org_helper.insert_one(new_dealer)

    async def get_orgs(self) -> Dict[str, OrganizationModel]:
        dealers = await org_helper.find_by_query({})
        return {str(d.id): d for d in dealers}
