from typing import Optional, Union, List, Dict, TypeVar, Generic
from datetime import datetime

from fastapi import HTTPException
from motor import motor_asyncio, core
from bson import ObjectId, Decimal128
from pydantic import BaseModel, Field, validator

from common.utils.db import PyDecimal, PyObjectId, clean_mongo_id, clean_mongo_dict, BasicDbFilter
from config import load_config

global_config = load_config()


def build_mongo_connection(collection_name=''):
    urls = global_config.get('MONGO_DB_URLS', {})
    url = urls.get(collection_name, {}).get('uri')
    if not url:
        url = urls.get('default', {}).get('uri')
    return motor_asyncio.AsyncIOMotorClient(url)


def build_mongo_collection(collection_name: str) -> core.AgnosticCollection:
    client = build_mongo_connection(collection_name)

    urls = global_config.get('MONGO_DB_URLS', {})
    db_name = urls.get(collection_name, {}).get('db')
    if not db_name:
        db_name = urls.get('default', {}).get('db')

    db = getattr(client, db_name)
    coll: core.AgnosticCollection = db.get_collection(collection_name)
    return coll


def build_mongo_db(collection_name: str) -> core.AgnosticDatabase:
    client = build_mongo_connection(collection_name)

    urls = global_config.get('MONGO_DB_URLS', {})
    db_name = urls.get(collection_name, {}).get('db')
    if not db_name:
        db_name = urls.get('default', {}).get('db')

    return getattr(client, db_name)


class BasePydanticConfig:
    orm_mode = True
    arbitrary_types_allowed = True
    json_encoders = {
        ObjectId: str,
        PyDecimal: str,
        Decimal128: str,
    }
    allow_population_by_field_name = True


class BasePydanticModel(BaseModel):
    class Config(BasePydanticConfig):
        pass


class BasePydanticTimestamped(BasePydanticModel):
    creation_timestamp: Optional[datetime]

    @validator("creation_timestamp", pre=True, always=True)
    def set_creation_timestamp(cls, creation_timestamp):
        return creation_timestamp or datetime.now()


class BaseDbModel(BasePydanticTimestamped):
    id: Optional[PyObjectId] = Field(alias='_id')
    archived: Optional[bool] = Field(default=False)
    deleted: Optional[bool] = Field(default=False)
    is_public: Optional[bool] = Field(default=False)


class DeleteOrArchive(BasePydanticModel):
    id: PyObjectId = Field(alias='_id')
    archived: Optional[bool]
    deleted: Optional[bool]


class UnDeleteOrUnArchive(BasePydanticModel):
    id: PyObjectId = Field(alias='_id')
    archived: Optional[bool]
    deleted: Optional[bool]


MODEL = TypeVar('MODEL')


class DbHelper(Generic[MODEL]):
    def __init__(self, name, model_class):
        self.collection = build_mongo_collection(name)
        self.model_class = model_class

    async def insert_one(self, model: Union[Dict, BaseModel]) -> MODEL:
        model_dict = clean_mongo_dict(model, exclude_unset=False)
        if 'id' in model_dict:
            del model_dict['id']
        if '_id' in model_dict:
            del model_dict['_id']
        new_model = await self.collection.insert_one(model_dict)
        new_model_dict = await self.collection.find_one({'_id': new_model.inserted_id})
        return self.model_class(**new_model_dict)

    async def update_one(self, model: Union[Dict, BaseModel], _id: Optional[Union[str, PyObjectId]] = None) -> MODEL:
        update_dict = clean_mongo_dict(model)
        if not _id:
            _id = update_dict.get('id', update_dict.get('_id'))
        if _id:
            _id = clean_mongo_id(_id)
            if update_dict.get('id'):
                del update_dict['id']
            elif update_dict.get('_id'):
                del update_dict['_id']
            await self.collection.update_one({"_id": _id}, {
                '$set': update_dict
            })
            return await self.find_one(_id)
        else:
            raise HTTPException(status_code=404, detail='Failed to find id in update query.')

    async def delete_or_archive(self, model: DeleteOrArchive):
        obj = await self.find_one(model.id)
        if obj:
            ret = obj
            if model.deleted:
                ret = await self.update_one({'_id': model.id, 'deleted': model.deleted})
            if model.archived:
                ret = await self.update_one({'_id': model.id, 'archived': model.archived})
            return ret

    async def actually_delete(self, _id: Union[str, ObjectId]):
        _id = clean_mongo_id(_id)
        await self.collection.delete_one({'_id': _id})
        return True

    async def undelete_or_unarchive(self, model: UnDeleteOrUnArchive):
        obj = await self.find_one(model.id)
        if obj:
            ret = obj
            if model.deleted is not None:
                ret = await self.update_one({'_id': model.id, 'deleted': model.deleted})
            if model.archived is not None:
                ret = await self.update_one({'_id': model.id, 'archived': model.archived})
            return ret

    async def find_one(self, _id: Union[str, ObjectId]) -> Optional[MODEL]:
        _id = clean_mongo_id(_id)
        model_dict = await self.collection.find_one({'_id': _id})
        if model_dict:
            return self.model_class(**model_dict)
        return None

    async def find_one_by_key(self, key: str, val: Union[str, ObjectId]) -> Optional[MODEL]:
        model_dict = await self.collection.find_one({key: val})
        if model_dict:
            return self.model_class(**model_dict)
        return None

    async def find_by_query(self, query: Union[Dict, BasicDbFilter], limit: int = 100) -> List[MODEL]:
        query_dict = query
        if isinstance(query, BasicDbFilter):
            query_dict = query.dump()
            limit = query.limit

        if query_dict.get('_id', None):
            query_dict['_id'] = clean_mongo_id(query_dict.get('_id'))

        docs = []
        cursor = self.collection.find(query_dict)
        for document in await cursor.to_list(length=limit):
            docs.append(self.model_class(**document))
        return docs

    async def find_by_query_single(self, query: Union[Dict, BasicDbFilter]) -> MODEL:
        docs = await self.find_by_query(query, limit=1)
        return docs[0] if docs else None

    async def find_all(self, limit: int = 100) -> List[MODEL]:
        docs = []
        cursor = self.collection.find()
        for document in await cursor.to_list(length=limit):
            docs.append(self.model_class(**document))
        return docs


class DbHelperFactory:
    @staticmethod
    def build(collection_name, model_class) -> DbHelper:
        return DbHelper(collection_name, model_class)


async def test_db():
    connection = build_mongo_connection()
    database = connection.test
    collection = database.get_collection("test_collection")

    test = await collection.insert_one({'test': "Testing"})
    new_test = await collection.find_one({'_id': test.inserted_id})
    print(new_test)
    return new_test


if __name__ == '__main__':
    import asyncio

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    result = loop.run_until_complete(test_db())
