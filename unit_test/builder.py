from config import load_config
from server import build_web_app
from web.routes.registration import web_routes
from web.middleware import middleware as mw

config = load_config()
app = build_web_app(config, web_routes, mw)
