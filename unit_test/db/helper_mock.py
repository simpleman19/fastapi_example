from db import DbHelper


class MockedDbHelper(DbHelper):
    # noinspection PyMissingConstructor
    def __init__(self, *args, **kwargs):
        # Stub
        self.ret_val = None

    def set_return_val(self, ret_val):
        self.ret_val = ret_val

    async def insert_one(self, *args, **kwargs):
        return self.ret_val

    async def update_one(self, *args, **kwargs):
        return self.ret_val

    async def find_one(self, *args, **kwargs):
        return self.ret_val

    async def find_one_by_key(self, *args, **kwargs):
        return self.ret_val

    async def find_by_query(self, *args, **kwargs):
        return self.ret_val

    async def find_by_query_single(self, *args, **kwargs):
        return self.ret_val

    async def find_all(self, *args, **kwargs):
        return self.ret_val


def build_and_mock_helper(mocker, path):
    mocked_db_helper = MockedDbHelper()
    mocker.patch(
        path,
        mocked_db_helper
    )
    return mocked_db_helper
