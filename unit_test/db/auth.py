from unit_test.db.helper_mock import build_and_mock_helper


def mock_auth_db_helper(mocker):
    from web.auth.user_models import user_helper
    return build_and_mock_helper(mocker, 'web.auth.service.user_helper')
