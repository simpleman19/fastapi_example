from fastapi.testclient import TestClient
from unit_test.builder import app

client = TestClient(app)


def test_health_check():
    response = client.get("/healthcheck")
    assert response.status_code == 200
    assert response.json() == {"message": "Healthy", "status": 200}
