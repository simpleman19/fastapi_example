import pytest
from fastapi.testclient import TestClient
from unit_test.builder import app
from unit_test.routes.auth.utils import patch_fake_user_into_service

client = TestClient(app)


@pytest.mark.asyncio
async def test_auth_route_login_pass(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/login", json={'username': 'testing', 'password': 'apassword'})
    assert response.status_code == 200
    assert response.json().get('token')


@pytest.mark.asyncio
async def test_auth_route_login_fail(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/login", json={'username': 'testing', 'password': 'notthepass'})
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_auth_route_update_password_valid(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/password",
                           json={
                               'username': 'testing',
                               'old_password': 'apassword',
                               'new_password': 'notthepass',
                               'one_time_code': '1234'
                           })
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_auth_route_update_password_invalid(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/password",
                           json={
                               'username': 'testing',
                               'old_password': '123apassword',
                               'new_password': 'notthepass',
                               'one_time_code': '1234'
                           })
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_auth_route_me_invalid(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/me")
    assert response.status_code == 405  # TODO I think this should be a 403


@pytest.mark.asyncio
async def test_auth_route_me_invalid2(mocker):
    patch_fake_user_into_service(mocker)

    response = client.post("/v1/auth/me", headers={'Authorization': 'failed test'})
    assert response.status_code == 405  # TODO I think this should be a 403
