from bson import ObjectId

from unit_test.db.auth import mock_auth_db_helper
from web.auth.service import hash_password
from web.auth.user_models import User


def patch_fake_user_into_service(mocker,
                                 username='testing',
                                 password='apassword',
                                 _id=ObjectId('121234561111234567891264')
                                 ):
    db_helper = mock_auth_db_helper(mocker)
    db_helper.ret_val = User(username=username)
    db_helper.ret_val.hashed_pass = hash_password(db_helper.ret_val, password)
    db_helper.ret_val.id = _id
    return db_helper.ret_val
