# from enum import Enum
# from typing import Set, Union, Type, Dict, Any
#
# from fastapi.openapi.constants import REF_PREFIX
# from pydantic import BaseModel
# from pydantic.schema import model_process_schema
#
#
# def _hacked_get_model_definitions(
#         *,
#         flat_models: Set[Union[Type[BaseModel], Type[Enum]]],
#         model_name_map: Dict[Union[Type[BaseModel], Type[Enum]], str],
# ) -> Dict[str, Any]:
#     definitions: Dict[str, Dict[str, Any]] = {}
#     for k, v in model_name_map.items():
#         model_name_map[k] = f'{k.__module__}__{k.__name__}'.replace('.', '__')
#     for model in flat_models:
#         m_schema, m_definitions, m_nested_models = model_process_schema(
#             model, model_name_map=model_name_map, ref_prefix=REF_PREFIX
#         )
#         definitions.update(m_definitions)
#         model_name = model_name_map[model]
#         definitions[model_name] = m_schema
#     return definitions
#
#
# import fastapi.openapi.utils
#
# fastapi.openapi.utils.get_model_definitions = _hacked_get_model_definitions_definitions
