from typing import Optional, Union, TYPE_CHECKING

import bcrypt
from fastapi import HTTPException

from common.utils.db import PyObjectId, clean_mongo_id
from web.auth.user_models import User, user_helper

if TYPE_CHECKING:
    from bson import ObjectId


def verify_password(password, hashed_password):
    if password:
        return bcrypt.checkpw(password.encode('utf-8'), hashed_password)
    else:
        return False


def hash_password(user: User, new_password, old_password=""):
    if not user.hashed_pass or verify_password(old_password, user.hashed_pass):
        return bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
    return None


async def create_user_in_db(user_in: User.Create, owner_id: Optional[Union[str, PyObjectId]] = None) -> User.Response:
    check_username = await user_helper.find_one_by_key('username', user_in.username)
    if check_username:
        raise HTTPException(status_code=403, detail="Username Already Exists")
    if not owner_id:
        owner_id = PyObjectId()
    owner_id = clean_mongo_id(owner_id)
    user_to_save = User(username=user_in.username, owner_id=owner_id)
    user_to_save.hashed_pass = hash_password(user_to_save, user_in.password)
    if not user_to_save.hashed_pass:
        raise HTTPException(status_code=403, detail="Failed to create user")
    mongo_user = await user_helper.insert_one(user_to_save)
    return User.Response(**mongo_user.dict(by_alias=True))


async def update_user_password_in_db(user: User, update_user: User.UpdatePass):
    hashed_pass = hash_password(user, update_user.new_password, update_user.old_password)
    if hashed_pass:
        await user_helper.update_one({'hashed_pass': hashed_pass}, user.id)
        return True
    else:
        return False


async def get_user_by_username(username: str) -> Optional[User]:
    return await user_helper.find_one_by_key('username', username)


async def get_user_by_id(user_id: Union['ObjectId', str]) -> Optional[User]:
    return await user_helper.find_one(user_id)
