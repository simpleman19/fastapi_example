from pydantic import BaseModel, Field
from typing import Optional

from common.utils.db import PyObjectId
from db import BaseDbModel, BasePydanticModel, DbHelperFactory, BasePydanticConfig, DbHelper

collection_name = 'users'
USERNAME_FIELD = Field(title='Username', min_length=6)


class User(BaseDbModel):
    owner_id: PyObjectId
    username: str = USERNAME_FIELD
    hashed_pass: Optional[bytes] = Field(title='Hashed Password')

    class Create(BaseModel):
        username: str = USERNAME_FIELD
        password: str = Field(title='Password', min_length=6)

        class Config(BasePydanticConfig):
            schema_extra = {
                "example": {
                    "username": "FooBar",
                    "password": "aReallySecurePass",
                },
            }
            title = 'User__Create'

    class LogIn(BaseModel):
        username: str = USERNAME_FIELD
        password: str = Field(title='Password', min_length=6)

        class Config(BasePydanticConfig):
            schema_extra = {
                "example": {
                    "username": "FooBar",
                    "password": "aReallySecurePass",
                }
            }
            title = 'User__Login'

    class UpdatePass(BaseModel):
        username: str = USERNAME_FIELD
        new_password: str = Field(title='New Password', min_length=6)
        old_password: Optional[str] = Field(title='Old Password')
        one_time_code: Optional[str] = Field(title='One Time Code')

        class Config(BasePydanticConfig):
            schema_extra = {
                "example": {
                    "username": "FooBar",
                    "old_password": "aReallySecurePass",
                    "new_password": "aReallySecurePass1",
                }
            }
            title = 'User__UpdatePass'

    class Response(BasePydanticModel):
        id: PyObjectId = Field(alias='_id')
        owner_id: PyObjectId
        username: str = USERNAME_FIELD

        class Config(BasePydanticConfig):
            title = 'User__Response'


user_helper: DbHelper[User] = DbHelperFactory.build(collection_name, User)


class SingleUserOut(BasePydanticModel):
    user: User.Response
    token: str


class TokenOut(BasePydanticModel):
    token: str
