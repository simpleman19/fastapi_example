from . import require_auth
from .user_models import User, SingleUserOut, TokenOut
from .service import create_user_in_db, get_user_by_username, get_user_by_id, verify_password, \
    update_user_password_in_db
from fastapi import Depends, HTTPException, APIRouter
from web.auth.jwt import JWTAuthorizationCredentials, JWTClaims

router = APIRouter(prefix='/auth', tags=['auth'])

unauthorized_exception = HTTPException(status_code=401, detail="Unauthorized")


@router.post('/create', status_code=201, response_model=SingleUserOut, responses={403: {}})
async def create_user(user_in: User.Create):
    ret_user = await create_user_in_db(user_in)
    access_token = require_auth.create_jwt_token(items=JWTClaims(
        **{"user_id": ret_user.id, "username": ret_user.username}
    ))
    return {'user': ret_user, 'token': access_token}


@router.post('/password', status_code=200, response_model=SingleUserOut)
async def update_user_password(user_in: User.UpdatePass):
    user = await get_user_by_username(user_in.username)
    if not user:
        raise unauthorized_exception
    success = await update_user_password_in_db(user, user_in)
    if not success:
        raise unauthorized_exception
    claim = JWTClaims(
        **{"user_id": user.id, "username": user.username}
    )
    access_token = require_auth.create_jwt_token(items=claim)
    return {'user': user, 'token': access_token}


@router.post('/login', status_code=200, response_model=TokenOut, responses={401: {}})
async def login(user_in: User.LogIn):
    user = await get_user_by_username(user_in.username)
    if not user or not verify_password(user_in.password, user.hashed_pass):
        raise unauthorized_exception
    claim = JWTClaims(
        **{"user_id": user.id, "username": user.username}
    )
    access_token = require_auth.create_jwt_token(items=claim)
    return {'token': access_token}


@router.get('/me', status_code=200, response_model=SingleUserOut, responses={401: {}})
async def jwt_identity(credentials: JWTAuthorizationCredentials = Depends(require_auth)):
    current_user_id = credentials.claims.get('user_id')
    user = None
    if current_user_id:
        user = await get_user_by_id(current_user_id)
    if user:
        token = require_auth.refresh_jwt_token(credentials)
        return {'user': user, 'token': token}
    else:
        raise unauthorized_exception


@router.post('/refresh', status_code=200, response_model=TokenOut, responses={401: {}})
async def refresh_jwt(credentials: JWTAuthorizationCredentials = Depends(require_auth)):
    access_token = require_auth.refresh_jwt_token(credentials)  # TODO handle expired token
    return {'token': access_token}
