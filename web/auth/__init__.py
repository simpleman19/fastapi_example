from web.auth.jwt import JWTBearer
from config import load_config

require_auth = JWTBearer(load_config())
