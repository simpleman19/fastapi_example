from typing import Optional, TYPE_CHECKING

from fastapi import Request, HTTPException

from . import require_auth

if TYPE_CHECKING:
    from .jwt import JWTAuthorizationCredentials


async def load_auth_middleware(request: Request):
    request.state.creds = None
    try:
        creds: Optional['JWTAuthorizationCredentials'] = await require_auth(request)
        request.state.creds = creds
    except HTTPException:
        pass
    return request
