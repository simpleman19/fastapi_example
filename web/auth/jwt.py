from calendar import timegm
from datetime import datetime, timedelta
from typing import Dict, Optional, Union

from fastapi import HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import jwt, JWTError, ExpiredSignatureError
from jose.exceptions import JWTClaimsError
from pydantic import BaseModel, Field
from starlette.requests import Request
from starlette.status import HTTP_403_FORBIDDEN

from common.utils import get_json_encodable_dict
from common.utils.db import PyObjectId
from config import CarSettings


def utc_for_token() -> int:
    return timegm(datetime.utcnow().utctimetuple())


class JWTAuthorizationCredentials(BaseModel):
    jwt_token: str
    header: Dict[str, str]
    claims: Dict[str, str]
    signature: str
    message: str


class JWTClaims(BaseModel):
    user_id: Union[PyObjectId, str]
    username: str
    exp: int = Field(default_factory=utc_for_token)


class JWTBearer(HTTPBearer):
    def __init__(self, settings: CarSettings, auto_error: bool = True):
        super().__init__(auto_error=auto_error)
        self.secret = settings.get('JWT_SECRET_TOKEN')
        self.token_expires_timedelta: timedelta = settings.get('JWT_TOKEN_EXPIRES')
        self.algo = settings.get('JWT_ALGORITHM')

    def verify_jwt_token(self, jwt_credentials: JWTAuthorizationCredentials) -> bool:
        try:
            jwt.decode(jwt_credentials.jwt_token, self.secret, self.algo)
        except ExpiredSignatureError:
            return False
        except JWTClaimsError:
            return False
        except JWTError:
            return False
        return True

    def verify_expired_jwt_token(self, jwt_credentials: JWTAuthorizationCredentials) -> bool:
        try:
            jwt.decode(jwt_credentials.jwt_token, self.secret, self.algo, options={'require_exp': True})
        except ExpiredSignatureError:
            return True  # TODO needs testing
        except JWTClaimsError:
            return False
        except JWTError:
            return False
        return True

    def create_jwt_token(self, items: JWTClaims) -> str:
        items.exp = utc_for_token() + self.token_expires_timedelta.total_seconds()
        item_dict = get_json_encodable_dict(items, exclude_unset=True)
        return jwt.encode(item_dict, self.secret, self.algo)

    def refresh_jwt_token(self, token: JWTAuthorizationCredentials) -> str:
        if not self.verify_expired_jwt_token(token):
            raise HTTPException(status_code=403, detail='Invalid Token Submitted')
        return self.create_jwt_token(
            JWTClaims(user_id=token.claims.get('user_id'), username=token.claims.get('username'))
        )

    async def __call__(self, request: Request) -> Optional[JWTAuthorizationCredentials]:
        if request.state.creds:  # TODO I think this method is raising a 405 instead of the 403
            return request.state.creds

        credentials: HTTPAuthorizationCredentials = await super().__call__(request)

        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(
                    status_code=HTTP_403_FORBIDDEN, detail="Wrong authentication method"
                )

            jwt_token = credentials.credentials

            message, signature = jwt_token.rsplit(".", 1)

            try:
                jwt_credentials = JWTAuthorizationCredentials(
                    jwt_token=jwt_token,
                    header=jwt.get_unverified_header(jwt_token),
                    claims=jwt.get_unverified_claims(jwt_token),
                    signature=signature,
                    message=message,
                )
            except JWTError:
                raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="JWT invalid")

            if not self.verify_jwt_token(jwt_credentials):
                raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="JWT invalid")

            return jwt_credentials
