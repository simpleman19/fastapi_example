from typing import Callable, List

from web.auth.middleware import load_auth_middleware

middleware: List[Callable] = [load_auth_middleware]
