from web.auth.routes import router as auth_router
from web.routes.files.v1 import router as files_router
from web.routes.files.v1_pub import router as files_pub_router
from web.routes.terminal.pending_v1 import router as pending_terminal_router
from web.routes.terminal.v1 import router as terminal_router
from web.routes.run_of_show.v1 import router as ros_router

web_routes = {
    '/v1': [
        auth_router,
        files_router,
        files_pub_router,
        terminal_router,
        pending_terminal_router,
        ros_router
    ],
}
