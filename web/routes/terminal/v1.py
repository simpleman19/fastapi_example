from typing import List

from fastapi import APIRouter, Depends, HTTPException

from common.custom_logger import get_logger
from common.utils import guid
from db import BasePydanticModel
from db.models.terminal.repo import TerminalType, Terminal, terminal_helper, TerminalDbFilter
from db.models.terminal.service import TerminalService
from web.auth.service import create_user_in_db
from web.auth.user_models import User

router = APIRouter(tags=['terminal'], prefix='/terminal', dependencies=[])

logger = get_logger(__name__)


class ClaimTerminal(BasePydanticModel):
    terminal_id: str
    terminal_type: TerminalType


class TerminalSecret(BasePydanticModel):
    id: str
    secret: str


class TerminalLogin(BasePydanticModel):
    id: str
    terminal_id: str
    secret: str


@router.get("/", response_model=List[Terminal.Response])
async def get_my_terminals(term_service: TerminalService = Depends(TerminalService)):
    return await term_service.my_terminals()


@router.get("/{_id}", response_model=Terminal.Response)
async def get_terminal(_id: str, term_service: TerminalService = Depends(TerminalService)):
    term = await term_service.get_terminal(_id)
    if not term:
        raise HTTPException(404, detail="Terminal Not Found")
    return term


@router.put("/{_id}", response_model=Terminal.Response)
async def update_terminal(_id: str, update: Terminal.Update, term_service: TerminalService = Depends(TerminalService)):
    return await term_service.update_term(_id, update)


@router.put("/secret/{_id}", response_model=Terminal.ResponseSecret)
async def create_terminal_secrets(_id: str):
    term = await terminal_helper.find_by_query_single(TerminalDbFilter(terminal_id=_id))
    if term.secret_hash:
        raise HTTPException(401, detail="Secret Error")
    secret = guid()
    await create_user_in_db(User.Create(username=term.terminal_id, password=secret), term.owner_id)
    resp = term.dict(by_alias=True)
    resp['secret'] = secret
    return resp


@router.get("/{_id}", response_model=Terminal.Response)
async def get_terminal(_id: str, term_service: TerminalService = Depends(TerminalService)):
    term = await term_service.get_terminal(_id)
    if not term:
        raise HTTPException(404, detail="Terminal Not Found")
    return term


@router.post("/claim", response_model=Terminal.Response)
async def claim_pending_terminal(terminal: ClaimTerminal,
                                 term_service: TerminalService = Depends(TerminalService)):
    return await term_service.claim_pending_terminal(terminal.terminal_id, terminal.terminal_type)
