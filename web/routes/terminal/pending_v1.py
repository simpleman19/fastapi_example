from fastapi import APIRouter, Depends

from common.custom_logger import get_logger
from db import BasePydanticModel
from db.models.terminal.service import PendingTerminalService

router = APIRouter(tags=['pending_terminal'], prefix='/terminal/pending', dependencies=[])

logger = get_logger(__name__)


class RegisterTerminal(BasePydanticModel):
    terminal_id: str


@router.post("/register")
async def register_pending_terminal(terminal: RegisterTerminal,
                                    term_service: PendingTerminalService = Depends(PendingTerminalService)):
    pending_term = await term_service.register_pending_terminal(terminal.terminal_id)
    return {"term": pending_term}
