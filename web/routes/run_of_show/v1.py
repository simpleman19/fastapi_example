from typing import List, Dict

from fastapi import APIRouter, Depends

from common.custom_logger import get_logger
from db.models.run_of_show.repo import RunOfShow, RosEntry
from db.models.run_of_show.service import RosService
from db.models.scene.repo import Scene
from db.models.scene.service import SceneService

router = APIRouter(tags=['ros'], prefix='/scene', dependencies=[])

logger = get_logger(__name__)


@router.get("/", response_model=List[Scene.Response])
async def my_scenes(scene_service: SceneService = Depends(SceneService)):
    return await scene_service.get_my_scenes()


@router.post("/", response_model=Scene.Response)
async def create_new_scene(scene: Scene.Create, scene_service: SceneService = Depends(SceneService)):
    return await scene_service.create_scene(scene)


@router.get("/{_id}", response_model=Scene.Response)
async def scene_by_id(_id: str, scene_service: SceneService = Depends(SceneService)):
    return await scene_service.get_scene(_id)


@router.get("/{_id}/ros", response_model=List[RunOfShow.Response])
async def all_ros_for_scene(_id: str, ros_service: RosService = Depends(RosService)):
    return await ros_service.get_ros_for_scene(_id)


@router.post("/{_id}/ros", response_model=RunOfShow.Response)
async def create_ros_for_scene(_id: str, ros: RunOfShow.Create, ros_service: RosService = Depends(RosService)):
    return await ros_service.create_ros(_id, ros)


@router.get("/{_id}/ros/{ros_id}", response_model=RunOfShow.Response)
async def ros_by_id_for_scene(_id: str, ros_id: str, ros_service: RosService = Depends(RosService)):
    return await ros_service.get_ros(_id, ros_id)


@router.post("/{_id}/ros/{ros_id}", response_model=RunOfShow.Response)
async def update_ros_for_scene(_id: str, ros_id: str, ros_update: RunOfShow.Update,
                               ros_service: RosService = Depends(RosService)):
    return await ros_service.update_ros(_id, ros_id, ros_update)


@router.post("/{_id}/ros/{ros_id}/entries", response_model=RunOfShow.Response)
async def update_ros_entries_for_scene(_id: str, ros_id: str, ros_entries: RunOfShow.RosUpdateEntries,
                                       ros_service: RosService = Depends(RosService)):
    return await ros_service.update_ros_entries(_id, ros_id, ros_entries)
