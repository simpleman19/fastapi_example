from typing import Union

from fastapi import APIRouter
from starlette.requests import Request

from common.custom_logger import get_logger
from common.utils.files import GridOutResponse
from db.models.files.images import PublicImageService
from db.models.files.videos import PublicVideoService, build_vid_response

router = APIRouter(tags=['pub/files'], prefix='/public/files', dependencies=[])

logger = get_logger(__name__)


@router.get("/attach/{_id}")
async def pub_attach_by_id(_id: str):
    gridout = await PublicImageService.get_image_stream(_id)
    return GridOutResponse(gridout, attachment=False)


@router.get("/img/{_id}")
async def pub_image_by_id(_id: str, w: Union[int, str] = 0, h: Union[int, str] = 0):
    gridout = await PublicImageService.get_image_stream(_id, w, h)
    return GridOutResponse(gridout, attachment=False)


@router.get("/vid/{_id}")
async def pub_video_by_id(_id: str, req: Request):
    grid_out = await PublicVideoService.get_video_stream(_id)
    return await build_vid_response(grid_out, req)
