from typing import List, Union

from fastapi import APIRouter, Depends, File, UploadFile, Form
from starlette.requests import Request

from common.custom_logger import get_logger
from common.utils.files import GridOutResponse
from db.files import FileModel
from db.models.files.attachments import attachment_helper, AttachmentService
from db.models.files.images import images_helper, ImageService, ImageModel
from db.models.files.videos import video_helper, build_vid_response, VideoService, VideoModel
from web.auth import require_auth

router = APIRouter(tags=['files'], prefix='/files', dependencies=[Depends(require_auth)])

logger = get_logger(__name__)


@router.post("/upload/att")
async def upload_attachment(file: UploadFile = File(...), public: bool = Form(False),
                            attach_service: AttachmentService = Depends(AttachmentService)):
    _id = await attach_service.save_attachment(file, public)
    return {"filenames": file.filename, 'id': str(_id)}


@router.post("/upload/img")
async def upload_image(file: UploadFile = File(...), public: bool = Form(False),
                       img_service: ImageService = Depends(ImageService)):
    _id = await img_service.save_image(file, public)
    return {"filenames": file.filename, 'id': str(_id)}


@router.post("/upload/vid")
async def upload_video(file: UploadFile = File(...), public: bool = Form(False),
                       vid_service: VideoService = Depends(VideoService)):
    _id = await vid_service.save_video(file, public)
    return {"filenames": file.filename, 'id': str(_id)}


@router.get("/att/all", response_model=List[FileModel.Response])
async def all_attachments():
    allthings = await attachment_helper.find_by_query({})
    return allthings


@router.get("/img/all", response_model=List[ImageModel.Response])
async def all_images():
    allthings = await images_helper.find_by_query({})
    return allthings


@router.get("/vid/all", response_model=List[VideoModel.Response])
async def all_videos():
    allthings = await video_helper.find_by_query({})
    return allthings


@router.get("/att/{_id}")
async def attachment_by_id(_id: str):
    gridout = await attachment_helper.download_stream(_id)
    return GridOutResponse(gridout, attachment=True)


@router.get("/img/{_id}")
async def image_by_id(_id: str, w: Union[int, str] = 0, h: Union[int, str] = 0, img_service: ImageService = Depends(ImageService)):
    gridout = await img_service.get_image_stream(_id, w, h)
    return GridOutResponse(gridout, attachment=False)


@router.get("/vid/{_id}")
async def video_by_id(_id: str, req: Request):
    grid_out = await video_helper.download_stream(_id)
    return build_vid_response(grid_out, req)
