/**
 * Apollo API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: dev
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */



import ApiClient from "../ApiClient";
import HealthCheckModel from '../model/HealthCheckModel';

/**
* Global service.
* @module apollo_client/GlobalApi
* @version dev
*/
export default class GlobalApi extends ApiClient {

    /**
    * Constructs a new GlobalApi. 
    * @alias module:apollo_client/GlobalApi
    * @class
    */
    constructor() {
      super();
      this.baseURL = null;
    }


    /**
     * Health Check
     * @return {Promise<HealthCheckModel>}
     */
    async getHealthCheck() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
        'User-Agent': 'OpenAPI-Generator/dev/Javascript',
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = HealthCheckModel;

      return this.callApi(
        '/healthcheck', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


}
