/**
 * Apollo API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: dev
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */



import ApiClient from "../ApiClient";
import AnyType from '../model/AnyType';
import HTTPValidationError from '../model/HTTPValidationError';
import RegisterTerminal from '../model/RegisterTerminal';

/**
* PendingTerminal service.
* @module apollo_client/PendingTerminalApi
* @version dev
*/
export default class PendingTerminalApi extends ApiClient {

    /**
    * Constructs a new PendingTerminalApi. 
    * @alias module:apollo_client/PendingTerminalApi
    * @class
    */
    constructor() {
      super();
      this.baseURL = null;
    }


    /**
     * Register Pending Terminal
     * @param {RegisterTerminal} registerTerminal 
     * @return {Promise<AnyType>}
     */
    async postRegisterPendingTerminal(registerTerminal) {
      let postBody = registerTerminal;
      // verify the required parameter 'registerTerminal' is set
      if (registerTerminal === undefined || registerTerminal === null) {
        throw new Error("Missing the required parameter 'registerTerminal' when calling postRegisterPendingTerminal");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
        'User-Agent': 'OpenAPI-Generator/dev/Javascript',
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = AnyType;

      return this.callApi(
        '/v1/terminal/pending/register', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


}
