/**
 * Apollo API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: dev
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import FileMetadata from './FileMetadata';

/**
 * The DbFilesFileModelResponse model module.
 * @module model/DbFilesFileModelResponse
 * @version dev
 */
class DbFilesFileModelResponse {
    /**
     * @member {String} _id
     * @type {String}
     */
    _id;
    /**
     * @member {Date} uploadDate
     * @type {Date}
     */
    uploadDate;
    /**
     * @member {String} filename
     * @type {String}
     */
    filename;
    /**
     * @member {Number} chunkSize
     * @type {Number}
     */
    chunkSize;
    /**
     * @member {Number} length
     * @type {Number}
     */
    length;
    /**
     * @member {FileMetadata} metadata
     * @type {FileMetadata}
     */
    metadata;

    

    /**
     * Constructs a new <code>DbFilesFileModelResponse</code>.
     * @alias module:model/DbFilesFileModelResponse
     * @param filename {String} 
     * @param chunkSize {Number} 
     * @param length {Number} 
     * @param metadata {FileMetadata} 
     */
    constructor(filename, chunkSize, length, metadata) { 
        
        DbFilesFileModelResponse.initialize(this, filename, chunkSize, length, metadata);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, filename, chunkSize, length, metadata) { 
        obj['filename'] = filename;
        obj['chunkSize'] = chunkSize;
        obj['length'] = length;
        obj['metadata'] = metadata;
    }

    /**
     * Constructs a <code>DbFilesFileModelResponse</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/DbFilesFileModelResponse} obj Optional instance to populate.
     * @return {module:model/DbFilesFileModelResponse} The populated <code>DbFilesFileModelResponse</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DbFilesFileModelResponse();

            if (data.hasOwnProperty('_id')) {
                obj['_id'] = ApiClient.convertToType(data['_id'], 'String');
            }
            if (data.hasOwnProperty('uploadDate')) {
                obj['uploadDate'] = ApiClient.convertToType(data['uploadDate'], 'Date');
            }
            if (data.hasOwnProperty('filename')) {
                obj['filename'] = ApiClient.convertToType(data['filename'], 'String');
            }
            if (data.hasOwnProperty('chunkSize')) {
                obj['chunkSize'] = ApiClient.convertToType(data['chunkSize'], 'Number');
            }
            if (data.hasOwnProperty('length')) {
                obj['length'] = ApiClient.convertToType(data['length'], 'Number');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = FileMetadata.constructFromObject(data['metadata']);
            }
        }
        return obj;
    }
}



export default DbFilesFileModelResponse;

