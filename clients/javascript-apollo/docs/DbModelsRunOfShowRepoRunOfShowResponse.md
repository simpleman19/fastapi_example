# Apollo.DbModelsRunOfShowRepoRunOfShowResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**ownerId** | **String** |  | 
**sceneId** | **String** |  | 
**name** | **String** |  | 
**desc** | **String** |  | 
**entries** | [**{String: RosEntry}**](RosEntry.md) |  | 


