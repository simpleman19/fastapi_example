# Apollo.VideoMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha256** | **String** |  | 
**fileExt** | **String** |  | 
**isPublic** | **Boolean** |  | [optional] [default to false]
**uploadedBy** | **String** |  | [optional] 
**width** | **Number** |  | 
**height** | **Number** |  | 
**duration** | **Number** |  | [optional] [default to 0]


