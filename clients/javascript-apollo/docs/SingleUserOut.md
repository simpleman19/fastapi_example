# Apollo.SingleUserOut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**WebAuthUserModelsUserResponse**](WebAuthUserModelsUserResponse.md) |  | 
**token** | **String** |  | 


