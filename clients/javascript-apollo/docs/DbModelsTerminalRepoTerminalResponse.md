# Apollo.DbModelsTerminalRepoTerminalResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | 
**terminalId** | **String** |  | 
**type** | [**TerminalType**](TerminalType.md) |  | 
**creationTimestamp** | **Date** |  | 
**config** | [**TerminalConfig**](TerminalConfig.md) |  | 


