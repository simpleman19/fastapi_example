# Apollo.TerminalConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displays** | [**{String: DisplayConfig}**](DisplayConfig.md) |  | [optional] 


