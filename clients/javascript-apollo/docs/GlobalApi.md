# Apollo.GlobalApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHealthCheck**](GlobalApi.md#getHealthCheck) | **GET** /healthcheck | Health Check



## getHealthCheck

> HealthCheckModel getHealthCheck()

Health Check

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.GlobalApi();
apiInstance.getHealthCheck((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**HealthCheckModel**](HealthCheckModel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

