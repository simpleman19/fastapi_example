# Apollo.HealthCheckModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | 
**status** | **Number** |  | 


