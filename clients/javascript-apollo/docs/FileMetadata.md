# Apollo.FileMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha256** | **String** |  | 
**fileExt** | **String** |  | 
**isPublic** | **Boolean** |  | [optional] [default to false]
**uploadedBy** | **String** |  | [optional] 


