# Apollo.PendingTerminalApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postRegisterPendingTerminal**](PendingTerminalApi.md#postRegisterPendingTerminal) | **POST** /v1/terminal/pending/register | Register Pending Terminal



## postRegisterPendingTerminal

> AnyType postRegisterPendingTerminal(registerTerminal)

Register Pending Terminal

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.PendingTerminalApi();
let registerTerminal = new Apollo.RegisterTerminal(); // RegisterTerminal | 
apiInstance.postRegisterPendingTerminal(registerTerminal, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerTerminal** | [**RegisterTerminal**](RegisterTerminal.md)|  | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

