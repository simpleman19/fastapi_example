# Apollo.DbModelsFilesVideosVideoModelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**uploadDate** | **Date** |  | [optional] 
**filename** | **String** |  | 
**chunkSize** | **Number** |  | 
**length** | **Number** |  | 
**metadata** | [**VideoMetadata**](VideoMetadata.md) |  | 


