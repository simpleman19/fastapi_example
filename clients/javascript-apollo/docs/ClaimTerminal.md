# Apollo.ClaimTerminal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terminalId** | **String** |  | 
**terminalType** | [**TerminalType**](TerminalType.md) |  | 


