# Apollo.PubFilesApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPubAttachById**](PubFilesApi.md#getPubAttachById) | **GET** /v1/public/files/attach/{_id} | Pub Attach By Id
[**getPubImageById**](PubFilesApi.md#getPubImageById) | **GET** /v1/public/files/img/{_id} | Pub Image By Id
[**getPubVideoById**](PubFilesApi.md#getPubVideoById) | **GET** /v1/public/files/vid/{_id} | Pub Video By Id



## getPubAttachById

> AnyType getPubAttachById(id)

Pub Attach By Id

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.PubFilesApi();
let id = "id_example"; // String | 
apiInstance.getPubAttachById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPubImageById

> AnyType getPubImageById(id, opts)

Pub Image By Id

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.PubFilesApi();
let id = "id_example"; // String | 
let opts = {
  'w': new Apollo.AnyOfintegerstring(), // AnyOfintegerstring | 
  'h': new Apollo.AnyOfintegerstring() // AnyOfintegerstring | 
};
apiInstance.getPubImageById(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **w** | [**AnyOfintegerstring**](.md)|  | [optional] 
 **h** | [**AnyOfintegerstring**](.md)|  | [optional] 

### Return type

[**AnyType**](AnyType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPubVideoById

> AnyType getPubVideoById(id)

Pub Video By Id

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.PubFilesApi();
let id = "id_example"; // String | 
apiInstance.getPubVideoById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

