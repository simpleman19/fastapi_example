# Apollo.DbModelsFilesImagesImageModelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**uploadDate** | **Date** |  | [optional] 
**filename** | **String** |  | 
**chunkSize** | **Number** |  | 
**length** | **Number** |  | 
**metadata** | [**ImageMetadata**](ImageMetadata.md) |  | 


