# Apollo.ApolloAction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionType** | **String** |  | [optional] [default to &#39;NOP&#39;]
**command** | [**{String: AnyOfstringobjectarray}**](AnyOfstringobjectarray.md) |  | [optional] 
**delayMillis** | **Number** |  | [optional] [default to 0]


