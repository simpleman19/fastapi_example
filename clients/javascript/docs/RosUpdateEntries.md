# Apollo.RosUpdateEntries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entries** | [**{String: RosEntry}**](RosEntry.md) |  | 


