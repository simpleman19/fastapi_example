# Apollo.RosEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**desc** | **String** |  | [optional] [default to &#39;Entry&#39;]
**actions** | [**{String: ApolloAction}**](ApolloAction.md) |  | [optional] 


