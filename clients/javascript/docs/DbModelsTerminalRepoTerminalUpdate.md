# Apollo.DbModelsTerminalRepoTerminalUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**type** | [**TerminalType**](TerminalType.md) |  | [optional] 
**config** | [**TerminalConfig**](TerminalConfig.md) |  | [optional] 


