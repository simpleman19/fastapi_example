# Apollo.DbModelsRunOfShowRepoRunOfShowUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**name** | **String** |  | [optional] 
**desc** | **String** |  | [optional] 


