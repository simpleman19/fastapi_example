# Apollo.ImageSized

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imageId** | **String** |  | [optional] 
**width** | **Number** |  | 
**height** | **Number** |  | 


