# Apollo.DisplayConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**width** | **Number** |  | [optional] [default to 1280]
**height** | **Number** |  | [optional] [default to 720]
**windowed** | **Boolean** |  | [optional] [default to true]
**contents** | [**[DisplayContent]**](DisplayContent.md) |  | [optional] 
**displayId** | **String** |  | [optional] [default to &#39;&#39;]


