# Apollo.ResponseSecret

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | 
**secret** | **String** |  | 
**terminalId** | **String** |  | 
**type** | [**TerminalType**](TerminalType.md) |  | 
**creationTimestamp** | **Date** |  | 
**config** | [**TerminalConfig**](TerminalConfig.md) |  | 


