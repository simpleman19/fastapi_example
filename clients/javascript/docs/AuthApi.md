# Apollo.AuthApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getJwtIdentity**](AuthApi.md#getJwtIdentity) | **GET** /v1/auth/me | Jwt Identity
[**postCreateUser**](AuthApi.md#postCreateUser) | **POST** /v1/auth/create | Create User
[**postLogin**](AuthApi.md#postLogin) | **POST** /v1/auth/login | Login
[**postRefreshJwt**](AuthApi.md#postRefreshJwt) | **POST** /v1/auth/refresh | Refresh Jwt
[**postUpdateUserPassword**](AuthApi.md#postUpdateUserPassword) | **POST** /v1/auth/password | Update User Password



## getJwtIdentity

> SingleUserOut getJwtIdentity()

Jwt Identity

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.AuthApi();
apiInstance.getJwtIdentity((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**SingleUserOut**](SingleUserOut.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postCreateUser

> SingleUserOut postCreateUser(webAuthUserModelsUserCreate)

Create User

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.AuthApi();
let webAuthUserModelsUserCreate = new Apollo.WebAuthUserModelsUserCreate(); // WebAuthUserModelsUserCreate | 
apiInstance.postCreateUser(webAuthUserModelsUserCreate, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webAuthUserModelsUserCreate** | [**WebAuthUserModelsUserCreate**](WebAuthUserModelsUserCreate.md)|  | 

### Return type

[**SingleUserOut**](SingleUserOut.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postLogin

> TokenOut postLogin(logIn)

Login

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.AuthApi();
let logIn = new Apollo.LogIn(); // LogIn | 
apiInstance.postLogin(logIn, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logIn** | [**LogIn**](LogIn.md)|  | 

### Return type

[**TokenOut**](TokenOut.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postRefreshJwt

> TokenOut postRefreshJwt()

Refresh Jwt

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.AuthApi();
apiInstance.postRefreshJwt((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**TokenOut**](TokenOut.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postUpdateUserPassword

> SingleUserOut postUpdateUserPassword(updatePass)

Update User Password

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.AuthApi();
let updatePass = new Apollo.UpdatePass(); // UpdatePass | 
apiInstance.postUpdateUserPassword(updatePass, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updatePass** | [**UpdatePass**](UpdatePass.md)|  | 

### Return type

[**SingleUserOut**](SingleUserOut.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

