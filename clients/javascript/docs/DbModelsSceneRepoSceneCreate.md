# Apollo.DbModelsSceneRepoSceneCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**desc** | **String** |  | [optional] [default to &#39;&#39;]
**location** | **String** |  | [optional] [default to &#39;&#39;]


