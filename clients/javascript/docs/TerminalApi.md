# Apollo.TerminalApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGetMyTerminals**](TerminalApi.md#getGetMyTerminals) | **GET** /v1/terminal/ | Get My Terminals
[**getGetTerminal**](TerminalApi.md#getGetTerminal) | **GET** /v1/terminal/{_id} | Get Terminal
[**postClaimPendingTerminal**](TerminalApi.md#postClaimPendingTerminal) | **POST** /v1/terminal/claim | Claim Pending Terminal
[**putCreateTerminalSecrets**](TerminalApi.md#putCreateTerminalSecrets) | **PUT** /v1/terminal/secret/{_id} | Create Terminal Secrets
[**putUpdateTerminal**](TerminalApi.md#putUpdateTerminal) | **PUT** /v1/terminal/{_id} | Update Terminal



## getGetMyTerminals

> [DbModelsTerminalRepoTerminalResponse] getGetMyTerminals()

Get My Terminals

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.TerminalApi();
apiInstance.getGetMyTerminals((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[DbModelsTerminalRepoTerminalResponse]**](DbModelsTerminalRepoTerminalResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGetTerminal

> DbModelsTerminalRepoTerminalResponse getGetTerminal(id)

Get Terminal

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.TerminalApi();
let id = "id_example"; // String | 
apiInstance.getGetTerminal(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**DbModelsTerminalRepoTerminalResponse**](DbModelsTerminalRepoTerminalResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postClaimPendingTerminal

> DbModelsTerminalRepoTerminalResponse postClaimPendingTerminal(claimTerminal)

Claim Pending Terminal

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.TerminalApi();
let claimTerminal = new Apollo.ClaimTerminal(); // ClaimTerminal | 
apiInstance.postClaimPendingTerminal(claimTerminal, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **claimTerminal** | [**ClaimTerminal**](ClaimTerminal.md)|  | 

### Return type

[**DbModelsTerminalRepoTerminalResponse**](DbModelsTerminalRepoTerminalResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCreateTerminalSecrets

> ResponseSecret putCreateTerminalSecrets(id)

Create Terminal Secrets

### Example

```javascript
import Apollo from 'apollo';

let apiInstance = new Apollo.TerminalApi();
let id = "id_example"; // String | 
apiInstance.putCreateTerminalSecrets(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**ResponseSecret**](ResponseSecret.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## putUpdateTerminal

> DbModelsTerminalRepoTerminalResponse putUpdateTerminal(id, dbModelsTerminalRepoTerminalUpdate)

Update Terminal

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.TerminalApi();
let id = "id_example"; // String | 
let dbModelsTerminalRepoTerminalUpdate = new Apollo.DbModelsTerminalRepoTerminalUpdate(); // DbModelsTerminalRepoTerminalUpdate | 
apiInstance.putUpdateTerminal(id, dbModelsTerminalRepoTerminalUpdate, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **dbModelsTerminalRepoTerminalUpdate** | [**DbModelsTerminalRepoTerminalUpdate**](DbModelsTerminalRepoTerminalUpdate.md)|  | 

### Return type

[**DbModelsTerminalRepoTerminalResponse**](DbModelsTerminalRepoTerminalResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

