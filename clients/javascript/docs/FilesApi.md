# Apollo.FilesApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllAttachments**](FilesApi.md#getAllAttachments) | **GET** /v1/files/att/all | All Attachments
[**getAllImages**](FilesApi.md#getAllImages) | **GET** /v1/files/img/all | All Images
[**getAllVideos**](FilesApi.md#getAllVideos) | **GET** /v1/files/vid/all | All Videos
[**getAttachmentById**](FilesApi.md#getAttachmentById) | **GET** /v1/files/att/{_id} | Attachment By Id
[**getImageById**](FilesApi.md#getImageById) | **GET** /v1/files/img/{_id} | Image By Id
[**getVideoById**](FilesApi.md#getVideoById) | **GET** /v1/files/vid/{_id} | Video By Id
[**postUploadAttachment**](FilesApi.md#postUploadAttachment) | **POST** /v1/files/upload/att | Upload Attachment
[**postUploadImage**](FilesApi.md#postUploadImage) | **POST** /v1/files/upload/img | Upload Image
[**postUploadVideo**](FilesApi.md#postUploadVideo) | **POST** /v1/files/upload/vid | Upload Video



## getAllAttachments

> [DbFilesFileModelResponse] getAllAttachments()

All Attachments

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
apiInstance.getAllAttachments((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[DbFilesFileModelResponse]**](DbFilesFileModelResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAllImages

> [DbModelsFilesImagesImageModelResponse] getAllImages()

All Images

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
apiInstance.getAllImages((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[DbModelsFilesImagesImageModelResponse]**](DbModelsFilesImagesImageModelResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAllVideos

> [DbModelsFilesVideosVideoModelResponse] getAllVideos()

All Videos

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
apiInstance.getAllVideos((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[DbModelsFilesVideosVideoModelResponse]**](DbModelsFilesVideosVideoModelResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAttachmentById

> Object getAttachmentById(id)

Attachment By Id

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let id = "id_example"; // String | 
apiInstance.getAttachmentById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getImageById

> Object getImageById(id, opts)

Image By Id

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let id = "id_example"; // String | 
let opts = {
  'w': new Apollo.AnyOfintegerstring(), // AnyOfintegerstring | 
  'h': new Apollo.AnyOfintegerstring() // AnyOfintegerstring | 
};
apiInstance.getImageById(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **w** | [**AnyOfintegerstring**](.md)|  | [optional] 
 **h** | [**AnyOfintegerstring**](.md)|  | [optional] 

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getVideoById

> Object getVideoById(id)

Video By Id

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let id = "id_example"; // String | 
apiInstance.getVideoById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postUploadAttachment

> Object postUploadAttachment(file, opts)

Upload Attachment

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let file = "/path/to/file"; // File | 
let opts = {
  '_public': false // Boolean | 
};
apiInstance.postUploadAttachment(file, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**|  | 
 **_public** | **Boolean**|  | [optional] [default to false]

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## postUploadImage

> Object postUploadImage(file, opts)

Upload Image

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let file = "/path/to/file"; // File | 
let opts = {
  '_public': false // Boolean | 
};
apiInstance.postUploadImage(file, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**|  | 
 **_public** | **Boolean**|  | [optional] [default to false]

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## postUploadVideo

> Object postUploadVideo(file, opts)

Upload Video

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.FilesApi();
let file = "/path/to/file"; // File | 
let opts = {
  '_public': false // Boolean | 
};
apiInstance.postUploadVideo(file, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**|  | 
 **_public** | **Boolean**|  | [optional] [default to false]

### Return type

**Object**

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

