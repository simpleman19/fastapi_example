# Apollo.RosApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllRosForScene**](RosApi.md#getAllRosForScene) | **GET** /v1/scene/{_id}/ros | All Ros For Scene
[**getMyScenes**](RosApi.md#getMyScenes) | **GET** /v1/scene/ | My Scenes
[**getRosByIdForScene**](RosApi.md#getRosByIdForScene) | **GET** /v1/scene/{_id}/ros/{ros_id} | Ros By Id For Scene
[**getSceneById**](RosApi.md#getSceneById) | **GET** /v1/scene/{_id} | Scene By Id
[**postCreateNewScene**](RosApi.md#postCreateNewScene) | **POST** /v1/scene/ | Create New Scene
[**postCreateRosForScene**](RosApi.md#postCreateRosForScene) | **POST** /v1/scene/{_id}/ros | Create Ros For Scene
[**postUpdateRosEntriesForScene**](RosApi.md#postUpdateRosEntriesForScene) | **POST** /v1/scene/{_id}/ros/{ros_id}/entries | Update Ros Entries For Scene
[**postUpdateRosForScene**](RosApi.md#postUpdateRosForScene) | **POST** /v1/scene/{_id}/ros/{ros_id} | Update Ros For Scene



## getAllRosForScene

> [DbModelsRunOfShowRepoRunOfShowResponse] getAllRosForScene(id)

All Ros For Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
apiInstance.getAllRosForScene(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**[DbModelsRunOfShowRepoRunOfShowResponse]**](DbModelsRunOfShowRepoRunOfShowResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMyScenes

> [DbModelsSceneRepoSceneResponse] getMyScenes()

My Scenes

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
apiInstance.getMyScenes((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[DbModelsSceneRepoSceneResponse]**](DbModelsSceneRepoSceneResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRosByIdForScene

> DbModelsRunOfShowRepoRunOfShowResponse getRosByIdForScene(id, rosId)

Ros By Id For Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
let rosId = "rosId_example"; // String | 
apiInstance.getRosByIdForScene(id, rosId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **rosId** | **String**|  | 

### Return type

[**DbModelsRunOfShowRepoRunOfShowResponse**](DbModelsRunOfShowRepoRunOfShowResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSceneById

> DbModelsSceneRepoSceneResponse getSceneById(id)

Scene By Id

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
apiInstance.getSceneById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**DbModelsSceneRepoSceneResponse**](DbModelsSceneRepoSceneResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postCreateNewScene

> DbModelsSceneRepoSceneResponse postCreateNewScene(dbModelsSceneRepoSceneCreate)

Create New Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let dbModelsSceneRepoSceneCreate = new Apollo.DbModelsSceneRepoSceneCreate(); // DbModelsSceneRepoSceneCreate | 
apiInstance.postCreateNewScene(dbModelsSceneRepoSceneCreate, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbModelsSceneRepoSceneCreate** | [**DbModelsSceneRepoSceneCreate**](DbModelsSceneRepoSceneCreate.md)|  | 

### Return type

[**DbModelsSceneRepoSceneResponse**](DbModelsSceneRepoSceneResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postCreateRosForScene

> DbModelsRunOfShowRepoRunOfShowResponse postCreateRosForScene(id, dbModelsRunOfShowRepoRunOfShowCreate)

Create Ros For Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
let dbModelsRunOfShowRepoRunOfShowCreate = new Apollo.DbModelsRunOfShowRepoRunOfShowCreate(); // DbModelsRunOfShowRepoRunOfShowCreate | 
apiInstance.postCreateRosForScene(id, dbModelsRunOfShowRepoRunOfShowCreate, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **dbModelsRunOfShowRepoRunOfShowCreate** | [**DbModelsRunOfShowRepoRunOfShowCreate**](DbModelsRunOfShowRepoRunOfShowCreate.md)|  | 

### Return type

[**DbModelsRunOfShowRepoRunOfShowResponse**](DbModelsRunOfShowRepoRunOfShowResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postUpdateRosEntriesForScene

> DbModelsRunOfShowRepoRunOfShowResponse postUpdateRosEntriesForScene(id, rosId, rosUpdateEntries)

Update Ros Entries For Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
let rosId = "rosId_example"; // String | 
let rosUpdateEntries = new Apollo.RosUpdateEntries(); // RosUpdateEntries | 
apiInstance.postUpdateRosEntriesForScene(id, rosId, rosUpdateEntries, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **rosId** | **String**|  | 
 **rosUpdateEntries** | [**RosUpdateEntries**](RosUpdateEntries.md)|  | 

### Return type

[**DbModelsRunOfShowRepoRunOfShowResponse**](DbModelsRunOfShowRepoRunOfShowResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postUpdateRosForScene

> DbModelsRunOfShowRepoRunOfShowResponse postUpdateRosForScene(id, rosId, dbModelsRunOfShowRepoRunOfShowUpdate)

Update Ros For Scene

### Example

```javascript
import Apollo from 'apollo';
let defaultClient = Apollo.ApiClient.instance;
// Configure Bearer access token for authorization: JWTBearer
let JWTBearer = defaultClient.authentications['JWTBearer'];
JWTBearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new Apollo.RosApi();
let id = "id_example"; // String | 
let rosId = "rosId_example"; // String | 
let dbModelsRunOfShowRepoRunOfShowUpdate = new Apollo.DbModelsRunOfShowRepoRunOfShowUpdate(); // DbModelsRunOfShowRepoRunOfShowUpdate | 
apiInstance.postUpdateRosForScene(id, rosId, dbModelsRunOfShowRepoRunOfShowUpdate, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **rosId** | **String**|  | 
 **dbModelsRunOfShowRepoRunOfShowUpdate** | [**DbModelsRunOfShowRepoRunOfShowUpdate**](DbModelsRunOfShowRepoRunOfShowUpdate.md)|  | 

### Return type

[**DbModelsRunOfShowRepoRunOfShowResponse**](DbModelsRunOfShowRepoRunOfShowResponse.md)

### Authorization

[JWTBearer](../README.md#JWTBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

