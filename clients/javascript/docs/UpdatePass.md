# Apollo.UpdatePass

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**newPassword** | **String** |  | 
**oldPassword** | **String** |  | [optional] 
**oneTimeCode** | **String** |  | [optional] 


