# Apollo.DisplayContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x** | **Number** |  | [optional] [default to 0]
**y** | **Number** |  | [optional] [default to 0]
**width** | **Number** |  | [optional] [default to 1280]
**height** | **Number** |  | [optional] [default to 720]
**type** | [**ContentType**](ContentType.md) |  | [optional] 


