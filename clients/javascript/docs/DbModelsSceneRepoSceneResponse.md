# Apollo.DbModelsSceneRepoSceneResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | 
**desc** | **String** |  | 
**startTime** | **Date** |  | 
**location** | **String** |  | 


