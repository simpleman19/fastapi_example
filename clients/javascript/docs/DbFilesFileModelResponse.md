# Apollo.DbFilesFileModelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**uploadDate** | **Date** |  | [optional] 
**filename** | **String** |  | 
**chunkSize** | **Number** |  | 
**length** | **Number** |  | 
**metadata** | [**FileMetadata**](FileMetadata.md) |  | 


