/**
 * Apollo API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: dev
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ImageSized model module.
 * @module model/ImageSized
 * @version dev
 */
class ImageSized {
    /**
     * Constructs a new <code>ImageSized</code>.
     * @alias module:model/ImageSized
     * @param width {Number} 
     * @param height {Number} 
     */
    constructor(width, height) { 
        
        ImageSized.initialize(this, width, height);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, width, height) { 
        obj['width'] = width;
        obj['height'] = height;
    }

    /**
     * Constructs a <code>ImageSized</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ImageSized} obj Optional instance to populate.
     * @return {module:model/ImageSized} The populated <code>ImageSized</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ImageSized();

            if (data.hasOwnProperty('image_id')) {
                obj['image_id'] = ApiClient.convertToType(data['image_id'], 'String');
            }
            if (data.hasOwnProperty('width')) {
                obj['width'] = ApiClient.convertToType(data['width'], 'Number');
            }
            if (data.hasOwnProperty('height')) {
                obj['height'] = ApiClient.convertToType(data['height'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {String} image_id
 */
ImageSized.prototype['image_id'] = undefined;

/**
 * @member {Number} width
 */
ImageSized.prototype['width'] = undefined;

/**
 * @member {Number} height
 */
ImageSized.prototype['height'] = undefined;






export default ImageSized;

