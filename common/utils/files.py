import pathlib
import hashlib
import os
import stat
from email.utils import formatdate
from mimetypes import guess_type
from urllib.parse import quote

from motor import motor_gridfs
from starlette.background import BackgroundTask
from starlette.responses import Response
from starlette.types import Receive, Scope, Send
from aiofiles.os import stat as aio_stat
import aiofiles
from tempfile import TemporaryFile

FILE_MIME_TYPES = {
    '.aac': {
        'ext': '.aac',
        'desc': 'AAC audio',
        'mime': 'audio/aac',
    },
    '.abw': {
        'ext': '.abw',
        'desc': 'AbiWord document',
        'mime': 'application/x-abiword',
    },
    '.arc': {
        'ext': '.arc',
        'desc': 'Archive document (multiple files embedded)',
        'mime': 'application/x-freearc',
    },
    '.avi': {
        'ext': '.avi',
        'desc': 'AVI: Audio Video Interleave',
        'mime': 'video/x-msvideo',
    },
    '.azw': {
        'ext': '.azw',
        'desc': 'Amazon Kindle eBook format',
        'mime': 'application/vnd.amazon.ebook',
    },
    '.bin': {
        'ext': '.bin',
        'desc': 'Any kind of binary data',
        'mime': 'application/octet-stream',
    },
    '.bmp': {
        'ext': '.bmp',
        'desc': 'Windows OS/2 Bitmap Graphics',
        'mime': 'image/bmp',
    },
    '.bz': {
        'ext': '.bz',
        'desc': 'BZip archive',
        'mime': 'application/x-bzip',
    },
    '.bz2': {
        'ext': '.bz2',
        'desc': 'BZip2 archive',
        'mime': 'application/x-bzip2',
    },
    '.csh': {
        'ext': '.csh',
        'desc': 'C-Shell script',
        'mime': 'application/x-csh',
    },
    '.css': {
        'ext': '.css',
        'desc': 'Cascading Style Sheets (CSS)',
        'mime': 'text/css',
    },
    '.csv': {
        'ext': '.csv',
        'desc': 'Comma-separated values (CSV)',
        'mime': 'text/csv',
    },
    '.doc': {
        'ext': '.doc',
        'desc': 'Microsoft Word',
        'mime': 'application/msword',
    },
    '.docx': {
        'ext': '.docx',
        'desc': 'Microsoft Word (OpenXML)',
        'mime': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    },
    '.eot': {
        'ext': '.eot',
        'desc': 'MS Embedded OpenType fonts',
        'mime': 'application/vnd.ms-fontobject',
    },
    '.epub': {
        'ext': '.epub',
        'desc': 'Electronic publication (EPUB)',
        'mime': 'application/epub+zip',
    },
    '.gz': {
        'ext': '.gz',
        'desc': 'GZip Compressed Archive',
        'mime': 'application/gzip',
    },
    '.gif': {
        'ext': '.gif',
        'desc': 'Graphics Interchange Format (GIF)',
        'mime': 'image/gif',
    },
    '.htm': {
        'ext': '.htm',
        'desc': 'HyperText Markup Language (HTML)',
        'mime': 'text/html',
    },
    '.html': {
        'ext': '.html',
        'desc': 'HyperText Markup Language (HTML)',
        'mime': 'text/html',
    },
    '.ico': {
        'ext': '.ico',
        'desc': 'Icon format',
        'mime': 'image/vnd.microsoft.icon',
    },
    '.ics': {
        'ext': '.ics',
        'desc': 'iCalendar format',
        'mime': 'text/calendar',
    },
    '.jar': {
        'ext': '.jar',
        'desc': 'Java Archive (JAR)',
        'mime': 'application/java-archive',
    },
    '.jpeg': {
        'ext': '.jpeg',
        'desc': 'JPEG images',
        'mime': 'image/jpeg',
    },
    '.jpg': {
        'ext': '.jpg',
        'desc': 'JPEG images',
        'mime': 'image/jpeg',
    },
    '.js': {
        'ext': '.js',
        'desc': 'JavaScript',
        'mime': 'text/javascript',
    },
    '.json': {
        'ext': '.json',
        'desc': 'JSON format',
        'mime': 'application/json',
    },
    '.jsonld': {
        'ext': '.jsonld',
        'desc': 'JSON-LD format',
        'mime': 'application/ld+json',
    },
    '.mid': {
        'ext': '.mid',
        'desc': 'Musical Instrument Digital Interface (MIDI)',
        'mime': 'audio/midi audio/x-midi',
    },
    '.midi': {
        'ext': '.midi',
        'desc': 'Musical Instrument Digital Interface (MIDI)',
        'mime': 'audio/midi audio/x-midi',
    },
    '.mjs': {
        'ext': '.mjs',
        'desc': 'JavaScript module',
        'mime': 'text/javascript',
    },
    '.mp3': {
        'ext': '.mp3',
        'desc': 'MP3 audio',
        'mime': 'audio/mpeg',
    },
    '.cda': {
        'ext': '.cda',
        'desc': 'CD audio',
        'mime': 'application/x-cdf',
    },
    '.mp4': {
        'ext': '.mp4',
        'desc': 'MP4 audio',
        'mime': 'video/mp4',
    },
    '.mpeg': {
        'ext': '.mpeg',
        'desc': 'MPEG Video',
        'mime': 'video/mpeg',
    },
    '.mpkg': {
        'ext': '.mpkg',
        'desc': 'Apple Installer Package',
        'mime': 'application/vnd.apple.installer+xml',
    },
    '.odp': {
        'ext': '.odp',
        'desc': 'OpenDocument presentation document',
        'mime': 'application/vnd.oasis.opendocument.presentation',
    },
    '.ods': {
        'ext': '.ods',
        'desc': 'OpenDocument spreadsheet document',
        'mime': 'application/vnd.oasis.opendocument.spreadsheet',
    },
    '.odt': {
        'ext': '.odt',
        'desc': 'OpenDocument text document',
        'mime': 'application/vnd.oasis.opendocument.text',
    },
    '.oga': {
        'ext': '.oga',
        'desc': 'OGG audio',
        'mime': 'audio/ogg',
    },
    '.ogv': {
        'ext': '.ogv',
        'desc': 'OGG video',
        'mime': 'video/ogg',
    },
    '.ogx': {
        'ext': '.ogx',
        'desc': 'OGG',
        'mime': 'application/ogg',
    },
    '.opus': {
        'ext': '.opus',
        'desc': 'Opus audio',
        'mime': 'audio/opus',
    },
    '.otf': {
        'ext': '.otf',
        'desc': 'OpenType font',
        'mime': 'font/otf',
    },
    '.png': {
        'ext': '.png',
        'desc': 'Portable Network Graphics',
        'mime': 'image/png',
    },
    '.pdf': {
        'ext': '.pdf',
        'desc': 'Adobe Portable Document Format (PDF)',
        'mime': 'application/pdf',
    },
    '.php': {
        'ext': '.php',
        'desc': 'Hypertext Preprocessor (Personal Home Page)',
        'mime': 'application/x-httpd-php',
    },
    '.ppt': {
        'ext': '.ppt',
        'desc': 'Microsoft PowerPoint',
        'mime': 'application/vnd.ms-powerpoint',
    },
    '.pptx': {
        'ext': '.pptx',
        'desc': 'Microsoft PowerPoint (OpenXML)',
        'mime': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    },
    '.rar': {
        'ext': '.rar',
        'desc': 'RAR archive',
        'mime': 'application/vnd.rar',
    },
    '.rtf': {
        'ext': '.rtf',
        'desc': 'Rich Text Format (RTF)',
        'mime': 'application/rtf',
    },
    '.sh': {
        'ext': '.sh',
        'desc': 'Bourne shell script',
        'mime': 'application/x-sh',
    },
    '.svg': {
        'ext': '.svg',
        'desc': 'Scalable Vector Graphics (SVG)',
        'mime': 'image/svg+xml',
    },
    '.swf': {
        'ext': '.swf',
        'desc': 'Small web format (SWF) or Adobe Flash document',
        'mime': 'application/x-shockwave-flash',
    },
    '.tar': {
        'ext': '.tar',
        'desc': 'Tape Archive (TAR)',
        'mime': 'application/x-tar',
    },
    '.tif': {
        'ext': '.tif',
        'desc': 'Tagged Image File Format (TIFF)',
        'mime': 'image/tiff',
    },
    '.tiff': {
        'ext': '.tiff',
        'desc': 'Tagged Image File Format (TIFF)',
        'mime': 'image/tiff',
    },
    '.ts': {
        'ext': '.ts',
        'desc': 'MPEG transport stream',
        'mime': 'video/mp2t',
    },
    '.ttf': {
        'ext': '.ttf',
        'desc': 'TrueType Font',
        'mime': 'font/ttf',
    },
    '.txt': {
        'ext': '.txt',
        'desc': 'Text',
        'mime': 'text/plain',
    },
    '.vsd': {
        'ext': '.vsd',
        'desc': 'Microsoft Visio',
        'mime': 'application/vnd.visio',
    },
    '.wav': {
        'ext': '.wav',
        'desc': 'Waveform Audio Format',
        'mime': 'audio/wav',
    },
    '.weba': {
        'ext': '.weba',
        'desc': 'WEBM audio',
        'mime': 'audio/webm',
    },
    '.webm': {
        'ext': '.webm',
        'desc': 'WEBM video',
        'mime': 'video/webm',
    },
    '.webp': {
        'ext': '.webp',
        'desc': 'WEBP image',
        'mime': 'image/webp',
    },
    '.woff': {
        'ext': '.woff',
        'desc': 'Web Open Font Format (WOFF)',
        'mime': 'font/woff',
    },
    '.woff2': {
        'ext': '.woff2',
        'desc': 'Web Open Font Format (WOFF)',
        'mime': 'font/woff2',
    },
    '.xhtml': {
        'ext': '.xhtml',
        'desc': 'XHTML',
        'mime': 'application/xhtml+xml',
    },
    '.xls': {
        'ext': '.xls',
        'desc': 'Microsoft Excel',
        'mime': 'application/vnd.ms-excel',
    },
    '.xlsx': {
        'ext': '.xlsx',
        'desc': 'Microsoft Excel (OpenXML)',
        'mime': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    },
    '.xml': {
        'ext': '.xml',
        'desc': 'XML',
        'mime': 'application/xml',
    },
    '.xul': {
        'ext': '.xul',
        'desc': 'XUL',
        'mime': 'application/vnd.mozilla.xul+xml',
    },
    '.zip': {
        'ext': '.zip',
        'desc': 'ZIP archive',
        'mime': 'application/zip',
    },
    '.3gp': {
        'ext': '.3gp',
        'desc': '3GPP audio/video container',
        'mime': 'video/3gpp',
    },
    '.3g2': {
        'ext': '.3g2',
        'desc': '3GPP2 audio/video container',
        'mime': 'video/3gpp2',
    },
    '.7z': {
        'ext': '.7z',
        'desc': '7-zip archive',
        'mime': 'application/x-7z-compressed',
    },

}


def sanitize_path(path):
    """
    Sanitize a path against directory traversals

    >>> sanitize_path('../test')
    'test'
    >>> sanitize_path('../../test')
    'test'
    >>> sanitize_path('../../abc/../test')
    'test'
    >>> sanitize_path('../../abc/../test/fixtures')
    'test/fixtures'
    >>> sanitize_path('../../abc/../.test/fixtures')
    '.test/fixtures'
    >>> sanitize_path('/test/foo')
    'test/foo'
    >>> sanitize_path('./test/bar')
    'test/bar'
    >>> sanitize_path('.test/baz')
    '.test/baz'
    >>> sanitize_path('qux')
    'qux'
    """
    # - pretending to chroot to the current directory
    # - cancelling all redundant paths (/.. = /)
    # - making the path relative
    return os.path.relpath(os.path.normpath(os.path.join("/", path)), "/")


def sanitize_filename(filename):
    temp = sanitize_path(filename)
    return os.path.basename(temp)


def get_filename_ext(filename):
    return pathlib.Path(filename).suffix


def filename_to_mime_type(filename):
    ext = get_filename_ext(filename)
    return FILE_MIME_TYPES[ext]


BUF_SIZE = 65536 * 2


def hash_file(file: TemporaryFile):
    sha256 = hashlib.sha256()
    while True:
        data = file.read(BUF_SIZE)
        if not data:
            break
        sha256.update(data)
    return sha256.hexdigest()


class GridOutResponse(Response):
    chunk_size = 4096

    def __init__(
            self,
            grid_out: motor_gridfs.AgnosticGridOut,
            status_code: int = 200,
            headers: dict = None,
            media_type: str = None,
            background: BackgroundTask = None,
            filename: str = None,
            method: str = None,
            attachment: bool = True,
            start_byte: int = 0,
            end_byte: int = 0
    ) -> None:
        self.grid_out = grid_out
        self.status_code = status_code

        if not filename:
            self.filename = grid_out.filename
        else:
            self.filename = filename

        self.send_header_only = method is not None and method.upper() == "HEAD"

        if media_type is None:
            media_type = filename_to_mime_type(self.filename)
            if not media_type:
                media_type = guess_type(self.filename)[0] or "text/plain"
            else:
                media_type = media_type.get('mime')

        self.media_type = media_type
        self.background = background
        self.init_headers(headers)

        self.attachment = attachment
        self.start_byte = start_byte
        if end_byte == 0:
            end_byte = self.grid_out.length
        self.end_byte = end_byte

        if self.filename is not None:
            content_disposition = ''
            if self.attachment:
                content_disposition += 'attachment; '
            content_disposition_filename = quote(self.filename)
            if content_disposition_filename != self.filename:
                content_disposition += "filename*=utf-8''{}".format(
                    content_disposition_filename
                )
            else:
                content_disposition += 'filename="{}"'.format(self.filename)
            self.headers.setdefault("content-disposition", content_disposition)

    def set_stat_headers(self) -> None:
        content_length = str(self.grid_out.length)
        timestamp = self.grid_out.upload_date.timestamp()
        last_modified = formatdate(timestamp, usegmt=True)
        if self.grid_out.metadata.get('sha256'):
            etag = self.grid_out.metadata.get('sha256')
        else:
            etag_base = str(timestamp) + "-" + str(self.grid_out.length)
            etag = hashlib.md5(etag_base.encode()).hexdigest()

        if self.end_byte - self.start_byte == content_length:
            self.headers.setdefault("content-length", content_length)
        else:
            self.headers.setdefault('Accept-Ranges', 'bytes')
            self.headers.setdefault('Content-Range', F"bytes {self.start_byte}-{self.end_byte}/{content_length}")

        self.headers.setdefault("last-modified", last_modified)
        self.headers.setdefault("etag", etag)

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        self.set_stat_headers()
        status_code = self.status_code
        if self.end_byte != self.grid_out.length:
            status_code = 206

        await send(
            {
                "type": "http.response.start",
                "status": status_code,
                "headers": self.raw_headers,
            }
        )
        if self.send_header_only:
            await send({"type": "http.response.body", "body": b"", "more_body": False})
        else:
            self.grid_out.seek(self.start_byte)
            bytes_read = 0

            while bytes_read < self.end_byte:
                chunk = await self.grid_out.read(self.chunk_size)
                bytes_read = bytes_read + len(chunk)
                await send(
                    {
                        "type": "http.response.body",
                        "body": chunk,
                        "more_body": bytes_read < self.end_byte,
                    }
                )

            self.grid_out.close()
        if self.background is not None:
            await self.background()
