from typing import Union, Dict, List, Optional, Tuple

from bson.errors import InvalidId
from bson import ObjectId, Decimal128
from pydantic import BaseModel, Field


class PyObjectId(ObjectId):

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError('Invalid objectid')
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')


class PyDecimal(Decimal128):

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if isinstance(v, str) and not str(v).replace('.', '', 1).isdecimal():
            raise ValueError('Invalid decimal')
        elif isinstance(v, Decimal128) or isinstance(v, PyDecimal):
            return v
        return Decimal128(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')


class BasicDbFilter(BaseModel):
    """
    id: PyObjectId id of the object unset to not filter by it
    archived: boolean should only return archived or not or unset to not filter by it
    deleted: boolean should only return deleted or not deleted or unset to not filter by it

    """
    _basic_col_filters = {'archived', 'deleted', 'is_public', 'id'}
    _extended_col_filters = {}
    id: Optional[PyObjectId] = Field(alias='_id')
    archived: Optional[bool]
    deleted: Optional[bool]
    is_public: Optional[bool]
    offset: Optional[int]
    limit: int = Field(default=100)
    sort_by: Optional[List[Tuple[str, int]]]

    @classmethod
    def default(cls):
        return cls(archived=False, deleted=False)

    def dump(self) -> Dict:
        return self.dict(by_alias=True,
                         exclude_unset=True,
                         include={
                             *self._basic_col_filters,
                             *self._extended_col_filters
                         })


def clean_mongo_id(_id: Union[str, PyObjectId]):
    if not isinstance(_id, PyObjectId):
        try:
            _id = PyObjectId(_id)
        except InvalidId:
            return None
    return _id


def clean_mongo_dict(input_dict: Union[Dict, BaseModel, List], by_alias=True, exclude_unset=True):
    if isinstance(input_dict, BaseModel):
        return input_dict.dict(by_alias=by_alias, exclude_unset=exclude_unset)
    elif isinstance(input_dict, Dict):
        for k, v in input_dict.items():
            input_dict[k] = clean_mongo_dict(v)
    elif isinstance(input_dict, List):
        input_dict = [clean_mongo_dict(v) for v in input_dict]
    return input_dict
