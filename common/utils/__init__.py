import importlib
import inspect
import json
import os
import uuid
import re
from datetime import datetime, date
from decimal import Decimal
from functools import lru_cache
from typing import List, Callable, Dict, Mapping

from bson import ObjectId
from pydantic import BaseModel

from common.custom_logger import get_logger

logger = get_logger(__name__)


def guid():
    return str(uuid.uuid4()).replace('-', '')


class ToDictMixin:
    excluded_fields: List

    def to_dict(self, include_props=True):
        ret_dict = {}
        excluded_fields = getattr(self, 'excluded_fields', [])
        fields = filter(lambda a: not a.startswith('__') and a not in excluded_fields, dir(self))
        if not include_props:
            fields = filter(lambda a: not isinstance(getattr(type(self), a, None), property), fields)
        for f in fields:
            val = getattr(self, f, None)
            if val and (isinstance(val, str) or isinstance(val, int)):
                ret_dict[f] = val
        return ret_dict


class FromDictMixin:
    excluded_fields: List

    @classmethod
    def from_dict(cls, source_dict: dict):
        obj = cls()
        fields = filter(lambda a: not a.startswith('__') and a, source_dict.keys())
        for f in fields:
            val = source_dict.get(f, None)
            if val:
                setattr(obj, f, val)
        return obj


def import_module_from_filename(base_module_path, dir_path, filename):
    # lets import the module
    base_package = dir_path.replace(base_module_path + os.sep, '').replace(os.sep, '.')
    filename_no_ext, _ = os.path.splitext(filename)
    module_path = base_package + '.' + filename_no_ext.replace(os.sep, '.')
    logger.debug(f"Dynamically importing module: {module_path}")
    return importlib.import_module(module_path)


def camel_to_snake(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


def json_serializer(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def pydantic_event_encoder(m):
    o = m
    if isinstance(m, BaseModel):
        o = m.dict(by_alias=True)
    return json.dumps(o, default=json_serializer).encode('utf-8')


def get_unique_id_for_callable(func: Callable):
    return hash(func)


def get_json_encodable_dict(model: BaseModel, exclude_unset=False):
    item_dict = model.dict(exclude_unset=exclude_unset)
    for k, v in item_dict.items():
        if isinstance(v, ObjectId):
            item_dict[k] = str(v)
    return item_dict


def fix_for_bson(item_dict: Dict):
    for k, v in item_dict.items():
        if isinstance(v, Decimal):
            item_dict[k] = str(v)
        if isinstance(v, dict):
            item_dict[k] = fix_for_bson(v)
        elif isinstance(v, list):
            item_dict[k] = []
            for x in v:
                item_dict[k].append(fix_for_bson(x))
    return item_dict


@lru_cache()
def build_type_to_kwarg_map(func: Callable) -> Dict:
    type_dict = {}
    params = get_parameters_from_func_sig(func)
    for name, param in params.items():
        annotation = param.annotation
        type_dict[name] = (annotation, param.default)

    return type_dict


@lru_cache()
def get_parameters_from_func_sig(func: Callable) -> Mapping[str, inspect.Parameter]:
    func_signature = inspect.signature(func)
    return func_signature.parameters
