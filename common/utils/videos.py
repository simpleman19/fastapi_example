from tempfile import TemporaryFile, NamedTemporaryFile
from common.utils.files import get_filename_ext
from pymediainfo import MediaInfo


def get_media_info(filepath: str):
    media_info = MediaInfo.parse(filepath)
    info_dict = {}
    for track in media_info.tracks:
        if track.track_type == "Video":
            info_dict.setdefault('video', {
                'width': track.width,
                'height': track.height,
                'duration': track.duration,
                'frame_rate': track.frame_rate,
                'format': track.format,
                'size': track.stream_size,
            })
        elif track.track_type == "Audio":
            info_dict.setdefault('audio', {
                'channels': track.channels,
                'duration': track.duration,
                'format': track.format,
                'size': track.stream_size,
                'language': track.language,
            })
    return info_dict


async def get_media_info_for_file(filename, file: TemporaryFile):
    ext = get_filename_ext(filename)
    file.seek(0)
    with NamedTemporaryFile(mode='w+b', suffix=ext) as tmp_file:
        tmp_file.write(file.read())
        tmp_file.seek(0)
        return get_media_info(tmp_file.name)
