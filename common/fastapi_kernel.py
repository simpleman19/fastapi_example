from typing import Optional

from fastapi import Depends, HTTPException
from starlette.requests import Request

from common.custom_logger import get_logger
from web.auth.jwt import JWTAuthorizationCredentials
from web.auth.user_models import User
from web.auth.service import get_user_by_id
from web.auth import require_auth

logger = get_logger(__name__)


class Kernel:
    def __init__(self):
        self.__user = None
        self.creds: Optional[JWTAuthorizationCredentials] = None

    async def get_user_optional(self) -> Optional[User]:
        if not self.creds:
            return None
        user_id = self.creds.claims.get('user_id')
        if not user_id:
            return None
        if not self.__user:
            self.__user = await get_user_by_id(user_id)
        return self.__user


class KernelAuthRequired(Kernel):
    def __init__(self, creds: JWTAuthorizationCredentials = Depends(require_auth)):
        logger.debug("Booting Kernel")
        super().__init__()
        self.creds: JWTAuthorizationCredentials = creds

    async def get_user(self) -> User:
        user = await self.get_user_optional()
        if not user:
            logger.warn(f"Authentication failed when loading user from creds.")
            raise HTTPException(status_code=401, detail="Unauthorized, User Not Found")
        return user


class KernelPublicDealer(Kernel):
    def __init__(self, request: Request):
        logger.debug("Booting Kernel")
        super().__init__()
        self.request: Request = request
