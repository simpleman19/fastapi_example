import os
from functools import lru_cache

from pydantic import BaseSettings

from common.custom_logger import get_logger

logger = get_logger(__name__)


class Config(BaseSettings):

    def get(self, name, default=None):
        return getattr(self, name, default)

    class Config:
        case_sensitive = False


class CarSettings(Config):
    SERVICE_NAME: str = "TODO SET NAME"
    SERVICE_VERSION = 'dev'

    URL_PREFIX = '/todo'
    MODEL_DIRS = ['models']
    MODEL_EXCLUDE_FILES = [
        'v1.py',
    ]
    KAFKA_URI = 'localhost:9092'
    SQLALCHEMY_DATABASE_URL = 'mysql+mysqlconnector://gardener:itsasecret@localhost/gardener'
    REDIS_HOST = 'localhost'
    REDIS_PASS = 'itsasecret'
    REDIS_PORT = 6379
    REDIS_DB = 0
    CORS_ORIGINS = []


@lru_cache()
def load_config(config_name=None) -> CarSettings:
    if not config_name:
        config_name = os.environ.get('APP_CONFIG', 'development')
    logger.info("Loading config: " + config_name + " config")
    config = config_map[config_name]()
    if not config:
        raise FileNotFoundError("Failed to find config: " + config_name)
    return config


def config_class(config_name):
    def inner(cls):
        config_map[config_name] = cls
        return cls

    return inner


config_map = {}
