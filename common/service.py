from fastapi import Depends

from common.custom_logger import get_logger
from common.fastapi_kernel import KernelAuthRequired, Kernel, KernelPublicDealer

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from web.auth.user_models import User

logger = get_logger(__name__)


class InjectableService:
    def __init__(self, kernel: Kernel = Depends(Kernel)):
        self.kernel: Kernel = kernel


class InjectableServiceAuthRequired:
    def __init__(self, kernel: KernelAuthRequired = Depends(KernelAuthRequired)):
        logger.debug("Booting Injectable Service")
        self.kernel = kernel

    async def get_user(self) -> 'User':
        return await self.kernel.get_user()


class InjectableServicePublicDealer:
    def __init__(self, kernel: KernelPublicDealer = Depends(KernelPublicDealer)):
        logger.debug("Booting Injectable Public Service")
        self.kernel = kernel
