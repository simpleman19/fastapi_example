import sys
import asyncio
import logging
import logging.handlers
from queue import SimpleQueue as Queue
from typing import List
import betterlogging

file_formatter = logging.Formatter('(%(asctime)s):%(levelname)s:%(name)s:%(funcName)s--%(message)s')
formatter = betterlogging.ColorizedFormatter(fmt='%(c_fg_green)s(%(asctime)s):%(c_color)s%(levelname)-1s%(c_reset)s:'
                                                 '%(c_fg_cyan)s%(name)s%(c_reset)s:%(funcName)s--'
                                                 '%(c_color)s%(message)s%(c_reset)s')

listener: logging.handlers.QueueListener


class LocalQueueHandler(logging.handlers.QueueHandler):
    def emit(self, record: logging.LogRecord) -> None:
        # Removed the call to self.prepare(), handle task cancellation
        try:
            self.enqueue(record)
        except asyncio.CancelledError:
            raise
        except Exception:
            self.handleError(record)


def setup_logging_queue() -> None:
    """Move log handlers to a separate thread.

    Replace handlers on the root logger with a LocalQueueHandler,
    and start a logging.QueueListener holding the original
    handlers.

    """
    global listener
    queue = Queue()
    root = logging.getLogger()

    handlers: List[logging.Handler] = []

    handler = LocalQueueHandler(queue)
    handler.setFormatter(formatter)
    root.addHandler(handler)
    for h in root.handlers[:]:
        if h is not handler:
            root.removeHandler(h)
            handlers.append(h)

    listener = logging.handlers.QueueListener(
        queue, *handlers, respect_handler_level=True
    )
    listener.start()


def shutdown_loggers():
    global listener
    listener.stop()


def setup_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Kill default handler to allow for root logger to be the only handler that catches
    log = logging.getLogger('werkzeug')
    if log:
        log.handlers = []


def redirect_to_file(filepath, mode='a'):
    logger = logging.getLogger()
    logger.handlers = []
    handler = logging.FileHandler(filepath, mode=mode)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def get_logger(name, log_level=logging.DEBUG):
    app_logger = logging.getLogger(name)
    app_logger.setLevel(log_level)
    return app_logger


# Auto configure root logger when imported
setup_logger()
setup_logging_queue()
