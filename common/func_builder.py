import types
import uuid
from functools import partial
from typing import Callable, List


def build_func(args: List[List[str]], func_handler: Callable, return_type=None) -> Callable:
    func_args = ""
    passing_args = ""
    decorator_args = "handler, "
    decorator_kwargs = {}
    for a in args:
        if len(a) == 0:
            continue
        func_args += f"{a[0]}"
        passing_args += f"{a[0]}={a[0]}, "
        if len(a) > 1:
            decorator_var_name = f"{a[0]}_annotation"
            func_args += f": {decorator_var_name}"
            decorator_args += f"{decorator_var_name}, "
            decorator_kwargs[decorator_var_name] = a[1]
        if len(a) > 2:
            decorator_var_name = f"{a[0]}_default"
            func_args += f" = {decorator_var_name}"
            decorator_args += f"{decorator_var_name}, "
            decorator_kwargs[decorator_var_name] = a[2]
        func_args += ", "

    return_type_str = ""
    if return_type:
        return_type_str = " -> return_type_var"
        decorator_args += "return_type_var, "
        decorator_kwargs["return_type_var"] = return_type

    func_args = func_args.rstrip(', ')
    passing_args = passing_args.rstrip(', ')
    decorator_args = decorator_args.rstrip(', ')

    code = """
def decorator({decorator_args}):
    async def f({args}){return_type}:
        return await handler({passing_args})
    return f
    """.format(args=func_args, passing_args=passing_args, decorator_args=decorator_args, return_type=return_type_str)
    module = types.ModuleType("dynamic_" + str(uuid.uuid4()).replace('-', ''))
    exec(code, module.__dict__)
    return module.decorator(handler=func_handler, **decorator_kwargs)


if __name__ == '__main__':
    def handler(**kwargs):
        print(kwargs)


    f = build_func([
        ["a", str],
        ["b", str],
        ["c", str],
        ["d", str, "Hello"],
    ], func_handler=handler)

    f('2', '4', '3')
