import os

import pytest
import typer
import uvicorn

from config import load_config

app = typer.Typer()


@app.command()
def runserver_dev(port: int = 8000):
    uvicorn_log_config = uvicorn.config.LOGGING_CONFIG
    uvicorn_log_config["loggers"] = []
    uvicorn_log_config["handlers"] = []
    uvicorn.run("server:app", host="0.0.0.0", port=port, reload=True, log_config=uvicorn_log_config, workers=3)


@app.command()
def runserver(port: int = 8000):
    uvicorn_log_config = uvicorn.config.LOGGING_CONFIG
    uvicorn_log_config["loggers"] = []
    uvicorn_log_config["handlers"] = []
    uvicorn.run("server:app", host="0.0.0.0", port=port, reload=False, log_config=uvicorn_log_config, workers=30)


# @app.command()
# def create_db():
#     settings = load_config()
#     engine = build_engine(settings)
#     Base.metadata.create_all(engine)


@app.command()
def unittest_dev():
    pytest.main(args=['-s', os.path.join(os.path.abspath(__file__), os.pardir, 'unit_test')])


@app.command()
def unittest():
    pytest.main(args=['-s', os.path.join(os.path.abspath(__file__), os.pardir, 'unit_test')])


if __name__ == "__main__":
    app()
