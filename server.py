import os.path

import web.hacks

from typing import List, Dict, Callable

from pydantic import BaseModel

from fastapi import FastAPI, Request, status
from starlette.middleware.base import BaseHTTPMiddleware
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.routing import APIRoute, APIRouter

from common.config import Config
from common.custom_logger import get_logger

logger = get_logger(__name__)


class HealthCheckModel(BaseModel):
    message: str
    status: int


def build_web_app(app_config: Config, routers: Dict[str, List[APIRouter]], middleware: List[Callable]):
    logger.info("Bootstrapping application.....")

    redocs_url = app_config.get('FASTAPI_REDOCS_URL')
    if redocs_url == 'Disabled' or not redocs_url:
        redocs_url = None
    docs_url = app_config.get('FASTAPI_DOCS_URL')
    if docs_url == 'Disabled' or not docs_url:
        docs_url = None

    fastapi = FastAPI(
        title=app_config.get('SERVICE_NAME'),
        version=app_config.get('SERVICE_VERSION'),
        docs_url=docs_url,
        redoc_url=redocs_url,
        servers=[
            {"url": "http://localhost:8000", "description": "Localhost"},
            {"url": "https://prod.example.com", "description": "Production environment"},
        ],
    )

    fastapi.add_middleware(
        CORSMiddleware,
        allow_origins=app_config.get('CORS_ORIGINS', []),
        allow_origin_regex=app_config.get('CORS_ORIGINS_REGEX', None),
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    logger.info("Mounting static directories...")
    fastapi.mount("/static", StaticFiles(directory=os.path.join(config.get('ROOT_DIR'), "static")), name="static")

    @fastapi.exception_handler(Exception)
    async def validation_exception_handler(request: Request, exc: Exception):
        logger.error("Exception: " + str(exc) + " Headers: " + str(request.headers))
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content=jsonable_encoder({"detail": 'Internal Server Error'}),
        )

    @fastapi.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        logger.error("Exception: " + str(exc) + " Headers: " + str(request.headers))
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": exc.errors(), "body": exc.body}),
        )

    logger.info("Adding in routers....")
    for api_version, to_register in routers.items():
        for r in to_register:
            logger.info(f"Adding in router: {r.tags}, prefix: {api_version}")
            fastapi.include_router(r, prefix=f"{api_version}")

    @fastapi.get('/healthcheck', tags=['global'], status_code=200, response_model=HealthCheckModel)
    def health_check():
        return {"message": "Healthy", "status": 200}

    logger.info("Building handler names...")
    use_route_names_as_operation_ids(fastapi)

    async def handle_custom_middleware(request: Request, call_next):
        for m in middleware:
            request = await m(request)
        response = await call_next(request)
        return response

    logger.info("Adding in handler for custom middlewares")
    fastapi.add_middleware(BaseHTTPMiddleware, dispatch=handle_custom_middleware)

    logger.info("Finished bootstrapping application...")
    return fastapi


def use_route_names_as_operation_ids(fast: FastAPI) -> None:
    """
    Simplify operation IDs so that generated API clients have simpler function
    names.

    Should be called only after all routes have been added.
    """
    for route in fast.routes:
        if isinstance(route, APIRoute):
            route.operation_id = f"{list(route.methods)[0].lower()}_{route.name}"


from config import load_config
from web.routes.registration import web_routes
from web.middleware import middleware as mw

config = load_config()
app = build_web_app(config, web_routes, mw)
