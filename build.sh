#!/bin/bash

pushd $(dirname ${BASH_SOURCE[0]})
export SERVICE=$(basename $PWD)
export SERVICE_VERSION=$(git describe --tags)

echo "docker build --build-arg SERVICE_NAME=$SERVICE --build-arg SERVICE_VERSION=$SERVICE_VERSION -t click_software/$SERVICE:$SERVICE_VERSION ."

docker build --build-arg SERVICE_NAME=$SERVICE --build-arg SERVICE_VERSION=$SERVICE_VERSION -t click_software/$SERVICE:$SERVICE_VERSION .

popd
