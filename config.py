import os
from datetime import timedelta

from jose.constants import ALGORITHMS
from pydantic import BaseModel

# load config import is here  to make available for other files to import from
from common.config import config_class, CarSettings, load_config

basedir = os.path.abspath(os.path.dirname(__file__))
rootdir = os.path.join(basedir, 'web')


class AuthMixin(BaseModel):
    JWT_SECRET_TOKEN: str = 'AReallySecretKey'
    JWT_TOKEN_EXPIRES: timedelta = timedelta(days=30)
    JWT_ALGORITHM: str = ALGORITHMS.HS256


@config_class('development')
class ApolloDevConfig(CarSettings, AuthMixin):
    SERVICE_NAME: str = "Apollo API"
    ROOT_DIR = rootdir
    KAFKA_URI = 'localhost:9092'
    MONGO_DB_URLS = {
        'default': {
            'uri': 'mongodb://apollo:itsasecret@localhost:27017/apollo?authSource=admin',
            'db': 'apollo'
        },
        'users': {
            'uri': 'mongodb://apollo:itsasecret@localhost:27017/auth?authSource=admin',
            'db': 'auth'
        },
        'images': {
            'uri': 'mongodb://apollo:itsasecret@localhost:27017/images?authSource=admin',
            'db': 'images'
        },
    }
    REDIS_HOST = 'localhost'
    REDIS_PASS = 'itsasecret'
    REDIS_PORT = 6379
    REDIS_DB = 0
    BASE_URLS = ['api.local:5000', 'web.local:5000']
    CORS_ORIGINS = [
        "http://web.local:5000",
        "http://api.local:5000",
    ]
    # CORS_ORIGINS_REGEX = 'http://.*\.local:5000'
    CORS_ORIGINS_REGEX = '.*'
    MAIL_RELAY = '10.0.0.50:30004'
    FASTAPI_DOCS_URL = '/docs'
    FASTAPI_REDOCS_URL = '/redoc'
    RAW_SCHEMA_ENABLED = True


@config_class('production')
class ApolloProdConfig(ApolloDevConfig):
    MONGO_DB_URLS = {
        'default': {
            'uri': 'mongodb://test2:testing@10.0.0.45:27017/apollo',
            'db': 'apollo'
        },
        'users': {
            'uri': 'mongodb://test:testing@10.0.0.45:27017/auth',
            'db': 'auth'
        },
        'images': {
            'uri': 'mongodb://test:testing@10.0.0.45:27017/images',
            'db': 'images'
        },
    }
    REDIS_HOST = 'localhost'
    REDIS_PASS = 'itsasecret'
    REDIS_PORT = 6379
    REDIS_DB = 0
    BASE_URLS = ['aads.online']
    CORS_ORIGINS = [
        "https://aads.online",
        "https://www.aads.online",
    ]
    CORS_ORIGINS_REGEX = 'https://.*\.aads\.online'

    FASTAPI_DOCS_URL = 'Disabled'
    FASTAPI_REDOCS_URL = 'Disabled'
    RAW_SCHEMA_ENABLED = False


@config_class('development')
class ApolloTestConfig(ApolloDevConfig):
    pass
