#!/bin/bash

pushd $(dirname ${BASH_SOURCE[0]})
export SERVICE=$(basename $PWD)
export SERVICE_VERSION=$(git describe --tags)

echo "pushing registry.therealcturner.com/car_app/$SERVICE:$SERVICE_VERSION"
docker image tag car_app/$SERVICE:$SERVICE_VERSION registry.therealcturner.com/car_app/$SERVICE:$SERVICE_VERSION
docker image tag car_app/$SERVICE:$SERVICE_VERSION registry.therealcturner.com/car_app/$SERVICE:latest
docker image push registry.therealcturner.com/car_app/$SERVICE:$SERVICE_VERSION
docker image push registry.therealcturner.com/car_app/$SERVICE:latest

popd
