import enum


class CarTitleTypes(str, enum.Enum):
    CLEAN = 'Clean'
    SALVAGE = 'Salvage'
    REBUILT = 'Rebuilt'


class CarConditionTypes(str, enum.Enum):
    NEW = 'New'
    USED = 'Used'
    REBUILT = 'Rebuilt'


class CarConditions(str, enum.Enum):
    C_1 = '1'
    C_2 = '2'
    C_3 = '3'
    C_4 = '4'
    C_5 = '5'
    C_6 = '6'
    C_7 = '7'
    C_8 = '8'
    C_9 = '9'
    C_10 = '10'


class CarBodyTypes(str, enum.Enum):
    SEDAN = 'Sedan'
    COUPE = 'Coupe'
    CROSSOVER = 'Crossover'
    HATCHBACK = 'Hatchback'
    PICKUP = 'Pickup'
    SEMI_TRUCK = 'Semi Truck'
    TRUCK = 'Truck'
    MOTORCYCLE = 'Motorcycle'
    TRICYCLE = 'Tricycle'
    TRAILER = 'Trailer'
    OTHER = 'Other'


class CarTransmissionTypes(str, enum.Enum):
    CVT = 'CVT'
    AUTOMATIC = 'Automatic'
    MANUAL = 'Manual'
    NA = 'N/A'


class DrivetrainTypes(str, enum.Enum):
    RWD = 'RWD'
    FWD = 'FWD'
    AWD = 'AWD'
    W4X4 = '4x4'
    OTHER = 'Other'


class CarFuelType(str, enum.Enum):
    GAS = 'Gasoline'
    DIESEL = 'Diesel'
    ELECTRIC = 'Electric'
    HYBRID = 'Hybrid'
    PLUG_IN_HYBRID = 'Plug In Hybrid'
    NATURAL_GAS = 'Natural Gas'
    OTHER = 'Other'


class MileageTypes(str, enum.Enum):
    ACTUAL = 'Actual'


class Colors(str, enum.Enum):
    N_A = 'N/A'
    AMBER = 'Amber'
    AQUA = 'Aqua'
    AZURE = 'Azure'
    BEIGE = 'Beige'
    BLACK = 'Black'
    BLUE = 'Blue'
    BLUE_GREEN = 'Blue Green'
    BLUE_VIOLET = 'Blue Violet'
    BRONZE = 'Bronze'
    BROWN = 'Brown'
    BURGUNDY = 'Burgundy'
    CAMOUFLAGE_GREEN = 'Camouflage Green'
    CHAMPAGNE = 'Champagne'
    CHARCOAL = 'Charcoal'
    CHOCOLATE = 'Chocolate'
    COPPER = 'Copper'
    CREAM = 'Cream'
    CYAN = 'Cyan'
    GOLD = 'Gold'
    GOLDENROD = 'Goldenrod'
    GREEN = 'Green'
    GREY = 'Grey'
    INDIGO = 'Indigo'
    IVORY = 'Ivory'
    JADE = 'Jade'
    KHAKI = 'Khaki'
    LAVENDER = 'Lavender'
    LIME_GREEN = 'Lime green'
    MAGENTA = 'Magenta'
    MAROON = 'Maroon'
    MIDNIGHT_BLUE = 'Midnight Blue'
    NAVY_BLUE = 'Navy Blue'
    OLIVE = 'Olive'
    ORANGE = 'Orange'
    PINK = 'Pink'
    PLATINUM = 'Platinum'
    POWDER_BLUE = 'Powder blue'
    PURPLE = 'Purple'
    QUARTZ_GREY = 'Quartz Grey'
    RED = 'Red'
    ROSE = 'Rose'
    ROYAL_BLUE = 'Royal Blue'
    ROYAL_PURPLE = 'Royal Purple'
    RUBY = 'Ruby'
    RUST = 'Rust'
    SAFETY_ORANGE = 'Safety Orange'
    SALMON = 'Salmon'
    SANDY_BROWN = 'Sandy brown'
    SAPPHIRE = 'Sapphire'
    SCARLET = 'Scarlet'
    SCHOOL_BUS_YELLOW = 'School Bus Yellow'
    SEA_GREEN = 'Sea Green'
    SILVER = 'Silver'
    SKY_BLUE = 'Sky Blue'
    SLATE_GREY = 'Slate Grey'
    TAN = 'Tan'
    TEAL = 'Teal'
    TITANIUM_WHITE = 'Titanium White'
    TURQUOISE = 'Turquoise'
    VIOLET = 'Violet'
    WHITE = 'White'
    YELLOW = 'Yellow'


CAR_MANUFACTURERS = {
    "Abarth": {
        'models': [

        ],
    },
    "Alfa Romeo": {
        'models': [

        ],
    },
    "Aston Martin": {
        'models': [

        ],
    },
    "Audi": {
        'models': [

        ],
    },
    "Bentley": {
        'models': [

        ],
    },
    "BMW": {
        'models': [

        ],
    },
    "Bugatti": {
        'models': [

        ],
    },
    "Cadillac": {
        'models': [

        ],
    },
    "Chevrolet": {
        'models': [
            'Cruze',
        ],
    },
    "Chrysler": {
        'models': [

        ],
    },
    "Citroën": {
        'models': [

        ],
    },
    "Dacia": {
        'models': [

        ],
    },
    "Daewoo": {
        'models': [

        ],
    },
    "Daihatsu": {
        'models': [

        ],
    },
    "Dodge": {
        'models': [

        ],
    },
    "Donkervoort": {
        'models': [

        ],
    },
    "DS": {
        'models': [

        ],
    },
    "Ferrari": {
        'models': [

        ],
    },
    "Fiat": {
        'models': [

        ],
    },
    "Fisker": {
        'models': [

        ],
    },
    "Ford": {
        'models': [

        ],
    },
    "Honda": {
        'models': [

        ],
    },
    "Hummer": {
        'models': [

        ],
    },
    "Hyundai": {
        'models': [

        ],
    },
    "Infiniti": {
        'models': [

        ],
    },
    "Iveco": {
        'models': [

        ],
    },
    "Jaguar": {
        'models': [

        ],
    },
    "Jeep": {
        'models': [

        ],
    },
    "Kia": {
        'models': [

        ],
    },
    "KTM": {
        'models': [

        ],
    },
    "Lada": {
        'models': [

        ],
    },
    "Lamborghini": {
        'models': [

        ],
    },
    "Lancia": {
        'models': [

        ],
    },
    "Land Rover": {
        'models': [

        ],
    },
    "Landwind": {
        'models': [

        ],
    },
    "Lexus": {
        'models': [

        ],
    },
    "Lotus": {
        'models': [

        ],
    },
    "Maserati": {
        'models': [

        ],
    },
    "Maybach": {
        'models': [

        ],
    },
    "Mazda": {
        'models': [

        ],
    },
    "McLaren": {
        'models': [

        ],
    },
    "Mercedes-Benz": {
        'models': [

        ],
    },
    "MG": {
        'models': [

        ],
    },
    "Mini": {
        'models': [

        ],
    },
    "Mitsubishi": {
        'models': [

        ],
    },
    "Morgan": {
        'models': [

        ],
    },
    "Nissan": {
        'models': [

        ],
    },
    "Opel": {
        'models': [

        ],
    },
    "Peugeot": {
        'models': [

        ],
    },
    "Porsche": {
        'models': [

        ],
    },
    "Renault": {
        'models': [

        ],
    },
    "Rolls-Royce": {
        'models': [

        ],
    },
    "Rover": {
        'models': [

        ],
    },
    "Saab": {
        'models': [

        ],
    },
    "Seat": {
        'models': [

        ],
    },
    "Skoda": {
        'models': [

        ],
    },
    "Smart": {
        'models': [

        ],
    },
    "SsangYong": {
        'models': [

        ],
    },
    "Subaru": {
        'models': [

        ],
    },
    "Suzuki": {
        'models': [

        ],
    },
    "Tesla": {
        'models': [

        ],
    },
    "Toyota": {
        'models': [

        ],
    },
    "Volkswagen": {
        'models': [

        ],
    },
    "Volvo": {
        'models': [

        ],
    },
    "Other": {
        'models': [
            'Other'
        ],
    },
}
